package broids.com.paynow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DemoActivity extends AppCompatActivity {

    Button btn_NFC, btn_Beacon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_acivity);

        btn_NFC = (Button) findViewById(R.id.btn_nfc);
        btn_NFC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(DemoActivity.this,DiscoveryActivity.class);
                startActivity(intent);
            }
        });
    }
}
