package broids.com.paynow.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import broids.com.paynow.R;
import broids.com.paynow.utils.AppPreferences;
import broids.com.paynow.utils.StringUtils;

public class AadhaarVerificationActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout llAlreadyHaveAccount;

    private TextView tvNext;

    private EditText etAadhaar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        checkIfAlreadyLoggedIn();
    }

    private void checkIfAlreadyLoggedIn() {

        String userId = AppPreferences.getString(AppPreferences.USERID, "", this);
        if(!StringUtils.isEmpty(userId)){

            HomeActivity.start(this);
            finish();
        }
    }

    @Override
    protected int getContentView() {

        return R.layout.activity_adhaar_verification;
    }

    @Override
    protected void initViews() {

        llAlreadyHaveAccount = (LinearLayout) findViewById(R.id.llAlreadyHaveAccount);
        llAlreadyHaveAccount.setOnClickListener(this);

        tvNext = (TextView) findViewById(R.id.tvNext);
        tvNext.setOnClickListener(this);

        etAadhaar = (EditText) findViewById(R.id.etAadharNo);
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.llAlreadyHaveAccount:
                login();
                break;

            case R.id.tvNext:
                otp();
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == OTPActivity.REQUEST_CODE) {

                register();
            }
        }
    }

    private void otp() {

        if (etAadhaar.getText().toString().length() != 12) {
            etAadhaar.setError("Please enter correct Aadhaar number");
            return;
        }

        String aadhaarNumber = etAadhaar.getText().toString();
        OTPActivity.startForResult(this, aadhaarNumber);
    }

    private void login() {

        LoginActivity.start(this);
        finish();
    }

    private void register() {

        RegisterActivity.start(this);
        finish();
    }

    public static void start(BaseActivity activity) {

        Intent intent = new Intent(activity, AadhaarVerificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.start(intent, true);
    }
}
