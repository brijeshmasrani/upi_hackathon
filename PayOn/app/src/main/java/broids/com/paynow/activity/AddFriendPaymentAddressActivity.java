package broids.com.paynow.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

import broids.com.paynow.R;
import broids.com.paynow.entity.FriendBean;
import broids.com.paynow.utils.FieldsValidator;

public class AddFriendPaymentAddressActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_CODE = 440;

    private Toolbar toolbar;

    private EditText etName;

    private EditText etPaymentAddress;

    @Override
    protected int getContentView() {
        return R.layout.activity_add_friends_payment_address;
    }

    @Override
    protected void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setVisibility(View.INVISIBLE);

        if (toolbar != null) {

            setSupportActionBar(toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();
                }
            });

            setTitle(R.string.add_friends);
        }

        etName = (EditText) findViewById(R.id.etName);
        etPaymentAddress = (EditText) findViewById(R.id.etPaymentAddress);

    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_done) {

            done();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void done() {

        if (!checkValidations()) {

            return;
        }

        String name = etName.getText().toString();
        String paymentAddress = etPaymentAddress.getText().toString();

        FriendBean friendBean = new FriendBean();
        friendBean.setName(name);
        friendBean.setPaymentAddress(paymentAddress);

        Random random = new Random();
        int red = random.nextInt();
        int green = random.nextInt();
        int blue = random.nextInt();
        int color = Color.rgb(red, green, blue);

        friendBean.setColorCode(color);

        friendBean.save();
        finish();
    }

    private boolean checkValidations() {

        return
                FieldsValidator.validatePersonNme(etName)
                        && FieldsValidator.validatePaymentAddress(etPaymentAddress, false);
    }

    public static void startForResult(BaseActivity activity) {

        Intent intent = new Intent(activity, AddFriendPaymentAddressActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.startForResult(intent, REQUEST_CODE, true);
    }
}
