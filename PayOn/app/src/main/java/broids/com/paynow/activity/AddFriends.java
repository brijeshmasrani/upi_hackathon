package broids.com.paynow.activity;

import android.app.ActionBar;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import broids.com.paynow.R;
import broids.com.paynow.adapter.SelectFriendAdapter;
import broids.com.paynow.entity.FriendBean;
import broids.com.paynow.utils.Logger;
import broids.com.paynow.utils.Notify;
import broids.com.paynow.utils.StringUtils;

public class AddFriends extends BaseActivity {

    private RecyclerView rvFriends;

    private SelectFriendAdapter adapter;

    private Toolbar toolbar;

    private View llSearch;

    private EditText etSearch;

    private ArrayList<FriendBean> friendBeans;

    @Override
    protected int getContentView() {

        return R.layout.activity_add_friends;
    }

    @Override
    protected void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setVisibility(View.INVISIBLE);

        if (toolbar != null) {

            setSupportActionBar(toolbar);

            toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();
                }
            });

            setTitle(getString(R.string.add_friends));
        }

        FloatingActionsMenu fabAdd = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        fabAdd.setVisibility(View.GONE);

        rvFriends = (RecyclerView) findViewById(R.id.rvFriends);

        adapter = new SelectFriendAdapter(this);
        rvFriends.setAdapter(adapter);

        adapter.setOnItemClickListener(new SelectFriendAdapter.OnItemClickListener() {

            @Override
            public void onItemSelected(FriendBean friendBean, int position) {

            }
        });

        rvFriends.setLayoutManager(new LinearLayoutManager(this));
        //DividerScroll.decorate(rvFriends, R.drawable.listing_broadcast_divider_theme1, false, true);

        llSearch = findViewById(R.id.llSearchView);

        ImageButton btnCloseSearch = (ImageButton) findViewById(R.id.btnCloseSearch);
        btnCloseSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                closeSearch();
            }
        });

        etSearch = (EditText) findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                onSearchText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void onSearchText(String s) {

        s = s.toLowerCase();

        if(StringUtils.isEmpty(s)) {

            adapter.setFriendBeans(friendBeans);
            return;
        }

        ArrayList<FriendBean> temp = new ArrayList<>();

        ArrayList<FriendBean> selectedFriends = adapter.getSelectedFriends();
        temp.addAll(selectedFriends);

        for(FriendBean friendBean : friendBeans) {

            String name = friendBean.getName().toLowerCase();

            if(selectedFriends.contains(friendBean)){

                continue;
            }

            if(name.startsWith(s)
                    || (s.length() > 2 && name.contains(s))){

                temp.add(friendBean);
            }
        }

        adapter.setFriendBeans(temp);
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

        showProgressDialog();

        friendBeans = new ArrayList<>();

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                ContentResolver cr = getContentResolver();

                Uri uri = ContactsContract.Contacts.CONTENT_URI;
                String sortOrder = ContactsContract.Contacts.DISPLAY_NAME;
                Cursor cur = cr.query(uri, null, null, null, sortOrder);

                if (cur.getCount() > 0) {

                    while (cur.moveToNext()) {

                        String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String photoId = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.PHOTO_ID));


                        int indexHasPhoneNumber = cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
                        boolean hasPhoneNumber = Integer.parseInt(cur.getString(indexHasPhoneNumber)) > 0;

                        FriendBean friendBean = new FriendBean();
                        friendBean.setName(name);
                        friendBean.setContactId(id);

                        if (photoId != null) {

                            Uri photoUri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, Long.parseLong(photoId));
                            friendBean.setPhotoUri(photoUri);
                        } else {

                            Random random = new Random();
                            int red = random.nextInt();
                            int green = random.nextInt();
                            int blue = random.nextInt();
                            int color = Color.rgb(red, green, blue);

                            friendBean.setColorCode(color);
                        }

                        friendBeans.add(friendBean);

                        //hasPhoneNumber = true;
                        if (hasPhoneNumber) {

                            Cursor pCur = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id}, null);

                            while (pCur.moveToNext()) {

                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                            }
                            pCur.close();
                        }
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {

                super.onPostExecute(aVoid);

                Collections.sort(friendBeans, new Comparator<FriendBean>() {

                    @Override
                    public int compare(FriendBean lhs, FriendBean rhs) {

                        try {
                            return lhs.getName().toLowerCase().compareTo(rhs.getName().toLowerCase());
                        } catch (Exception e) {
                            Logger.e(e);
                        }

                        return 0;
                    }
                });

                adapter.setFriendBeans(friendBeans);
                dismissProgressDialog();
            }
        }.execute();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_add_friend, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_done) {

            done();

            return true;

        } else if (id == R.id.action_search) {

            search();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void search() {

        llSearch.setVisibility(View.VISIBLE);

        /*ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.hide();
        }*/

        toolbar.setVisibility(View.GONE);

        etSearch.requestFocus();

    }


    private void closeSearch() {

        llSearch.setVisibility(View.GONE);

        /*ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.show();
        }*/

        toolbar.setVisibility(View.VISIBLE);

        adapter.setFriendBeans(friendBeans);
        etSearch.setText(null);
    }

    private void done() {

        ArrayList<FriendBean> selectedFriends = adapter.getSelectedFriends();

        if(selectedFriends.size() <= 0) {

            String text = getString(R.string.nothing_added);
            Notify.toast(text, this);
        }

        for(FriendBean friendBean : selectedFriends) {

            friendBean.save();
        }

        finish();
    }

}
