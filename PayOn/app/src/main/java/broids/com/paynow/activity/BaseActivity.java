package broids.com.paynow.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import broids.com.paynow.R;
import broids.com.paynow.utils.Logger;
import broids.com.paynow.utils.ProgressDialogProvider;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public abstract class BaseActivity extends AppCompatActivity {

    //protected ProgressDialog progressDialog;
    protected Dialog progressDialog;

    private boolean isRecreating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(getContentView());

        initViews();

        setTextLabels();

        loadData();

        setAppTheme();
    }

    protected abstract int getContentView();

    protected abstract void initViews();

    protected abstract void setTextLabels();

    public abstract void setAppTheme();

    protected abstract void loadData();

    @Override
    protected void attachBaseContext(Context newBase) {

        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void showProgressDialog() {

        if (progressDialog == null) {

            //progressDialog = ProgressDialogProvider.getSimpleProgressDialog(title, message, this);
            progressDialog = ProgressDialogProvider.getSimpleProgressDialog(this);
            progressDialog.setCancelable(false);

        } else {

            //progressDialog.setTitle(title);
            //progressDialog.setMessage(message);
        }

        progressDialog.show();
    }


    public void dismissProgressDialog() {

        try {

            if (progressDialog != null && progressDialog.isShowing()) {

                progressDialog.dismiss();
            }

        } catch (Exception e) {

            Logger.e(e);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Logger.e("onActivityResult", "onActivityResult : " + requestCode);
    }

    protected void start(Class activityClass, Bundle bundle, boolean shouldAnimate) {

        Intent intent = new Intent(this, activityClass);

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        startActivity(intent);
        animateNext(shouldAnimate);
    }

    public void start(Intent intent, boolean shouldAnimate) {

        startActivity(intent);
        animateNext(shouldAnimate);
    }

    public void startForResult(Intent intent, int code, boolean shouldAnimate) {

        startActivityForResult(intent, code);
        animateNext(shouldAnimate);
    }

    public void startForResult(Intent intent, Bundle bundle, int code, boolean shouldAnimate) {

        if (bundle != null) {
            intent.putExtras(bundle);
        }

        startActivityForResult(intent, code);
        animateNext(shouldAnimate);
    }

    private void animateNext(boolean shouldAnimate) {

        int in = R.anim.enter;
        int out = R.anim.exit;
        int pop_in = R.anim.pop_enter;
        int pop_out = R.anim.pop_exit;

        if (shouldAnimate) {

            overridePendingTransition(in, out);
        }
    }


    @Override
    public void finish() {

        super.finish();
    }
}
