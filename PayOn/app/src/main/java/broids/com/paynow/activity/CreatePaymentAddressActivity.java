package broids.com.paynow.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import broids.com.paynow.R;
import broids.com.paynow.entity.BankAccountBean;
import broids.com.paynow.entity.PaymentAddressBean;
import broids.com.paynow.utils.FieldsValidator;
import broids.com.paynow.utils.Notify;

public class CreatePaymentAddressActivity extends BaseActivity implements View.OnClickListener {

    public static final int REQUEST_CODE = 100;

    private static final String PAYMENT_ADDRESS = "Payment Address";

    private TextView tvNext;

    private TextView tvLinkPaymentAddress;

    private EditText etPaymentAddress;

    private EditText etIFSCCode;

    private EditText etAccountNumber;

    private EditText etReAccountNumber;

    private TextView tvSetupPaymentAddressLabel;

    private ArrayList<String> addresses = new ArrayList<>();

    @Override
    protected int getContentView() {

        return R.layout.activity_create_payment_address;
    }

    @Override
    protected void initViews() {

        tvNext = (TextView) findViewById(R.id.tvNext);
        tvNext.setOnClickListener(this);

        etIFSCCode = (EditText) findViewById(R.id.etIFSCCode);
        etAccountNumber = (EditText) findViewById(R.id.etAccountNumber);
        etReAccountNumber = (EditText) findViewById(R.id.etReAccountNumber);

        etPaymentAddress = (EditText) findViewById(R.id.etPaymentAddress);

        InputFilter filter = new InputFilter() {

            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {

                for (int i = start; i < end; i++) {

                    char character = source.charAt(i);

                    if ((!Character.isLetterOrDigit(character))
                            && character != '.'
                            && character != '-') {
                        return "";
                    }
                }
                return null;
            }
        };

        etPaymentAddress.setFilters(new InputFilter[]{filter});

        etPaymentAddress.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (FieldsValidator.validatePaymentAddress(etPaymentAddress, true)) {

                    String mapp = getString(R.string.mapp);

                    etPaymentAddress.setTextColor(getResources().getColor(R.color.grey_text));

                    if (addresses.contains(s + mapp)) {

                        //btnCheck.setImageResource(R.drawable.add_inactive);
                        //btnCheck.setEnabled(false);

                        String msg = getString(R.string.validation_payment_address_exists);
                        Notify.showSnackBar(etPaymentAddress, msg, CreatePaymentAddressActivity.this);

                        etPaymentAddress.setTextColor(getResources().getColor(R.color.colorAccent));

                        return;
                    }

                    //btnCheck.setImageResource(R.drawable.adda_active);
                    //btnCheck.setEnabled(true);

                } else {

                    //btnCheck.setImageResource(R.drawable.add_inactive);
                    //btnCheck.setEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                etPaymentAddress.removeTextChangedListener(this);
                etPaymentAddress.setText(s.toString().toLowerCase());
                etPaymentAddress.setSelection(etPaymentAddress.getText().length());
                etPaymentAddress.addTextChangedListener(this);
            }
        });

        tvSetupPaymentAddressLabel = (TextView) findViewById(R.id.tvSetupPaymentAddressLabel);

        tvLinkPaymentAddress = (TextView) findViewById(R.id.tvLinkPaymentAddress);
        tvLinkPaymentAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinkPaymentAddressActivity.start(CreatePaymentAddressActivity.this);
            }
        });
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

        Bundle bundle = getIntent().getExtras();

        if(bundle != null) {

            if(bundle.containsKey(PAYMENT_ADDRESS)) {

                String paymentAddress = bundle.getString(PAYMENT_ADDRESS);

                if (paymentAddress != null) {

                    String mapp = getString(R.string.mapp);

                    etPaymentAddress.setText(paymentAddress.replace(mapp, ""));
                }
            }
        }

        addresses = PaymentAddressBean.getAllAddresses();

        boolean isFirstTime = addresses.size() == 0;
        if (isFirstTime) {

            tvSetupPaymentAddressLabel.setText(R.string.set_up_first_payment_address);
            tvNext.setText(R.string.next);

        } else {

            tvSetupPaymentAddressLabel.setText(R.string.set_up_payment_address);
            tvNext.setText(R.string.save);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tvNext:
                next();
                break;

        }
    }

    private void next() {

        if (checkValidations()) {

            String mapp = getString(R.string.mapp);

            String paymentAddress = etPaymentAddress.getText().toString().trim() + mapp;
            String accNo = etAccountNumber.getText().toString().trim();
            String ifsc = etIFSCCode.getText().toString().trim();

            PaymentAddressBean paymentAddressBean = new PaymentAddressBean();
            paymentAddressBean.setPaymentAddress(paymentAddress);
            paymentAddressBean.save();

            BankAccountBean bankAccountBean = new BankAccountBean();
            bankAccountBean.setPaymentAddress(paymentAddress);
            bankAccountBean.setAccountNumber(accNo);
            bankAccountBean.setIfscCode(ifsc);
            bankAccountBean.save();

            setResult(RESULT_OK);
            finish();
        }

    }

    private boolean checkValidations() {

        // Existing payment address will replaced
        /*String mapp = getString(R.string.mapp);

        if (addresses.contains(etPaymentAddress.getText() + mapp)) {

            String msg = getString(R.string.validation_payment_address_exists);
            Notify.toast(msg, this);
            return false;
        }*/

        return FieldsValidator.validateIFSC(etIFSCCode)
                && FieldsValidator.validateBankAccNumber(etAccountNumber)
                && FieldsValidator.validateBankAccNumber(etReAccountNumber)
                && FieldsValidator.matchAccountNumbers(etAccountNumber, etReAccountNumber)
                && FieldsValidator.validatePaymentAddress(etPaymentAddress, false);
    }

    public static void startForResult(BaseActivity activity) {

        Intent intent = new Intent(activity, CreatePaymentAddressActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.startForResult(intent, REQUEST_CODE, true);
    }

    public static void startForResult(String paymentAddress, BaseActivity activity) {

        Intent intent = new Intent(activity, CreatePaymentAddressActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra(PAYMENT_ADDRESS, paymentAddress);

        activity.startForResult(intent, REQUEST_CODE, true);
    }
}
