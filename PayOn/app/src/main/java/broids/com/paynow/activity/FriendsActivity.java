package broids.com.paynow.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;

import broids.com.paynow.R;
import broids.com.paynow.adapter.FriendsListAdapter;
import broids.com.paynow.entity.FriendBean;
import broids.com.paynow.utils.StringUtils;

public class FriendsActivity extends BaseActivity {

    public static final String SELECTED_FRIEND = "selected_friend";

    public static final int REQUEST_CODE = 1000;

    private RecyclerView rvFriends;

    private FriendsListAdapter adapter;

    private Toolbar toolbar;

    private View llSearch;

    private EditText etSearch;

    private ArrayList<FriendBean> friendBeans;

    @Override
    protected int getContentView() {

        return R.layout.activity_add_friends;
    }

    @Override
    protected void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setVisibility(View.INVISIBLE);

        if (toolbar != null) {

            setSupportActionBar(toolbar);

            toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();
                }
            });

            setTitle(getString(R.string.friends));
        }

        rvFriends = (RecyclerView) findViewById(R.id.rvFriends);

        adapter = new FriendsListAdapter(this);
        rvFriends.setAdapter(adapter);

        rvFriends.setLayoutManager(new LinearLayoutManager(this));
        //DividerScroll.decorate(rvFriends, R.drawable.listing_broadcast_divider_theme1, false, true);

        final FloatingActionsMenu multiple_actions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);

        FloatingActionButton fabByPaymentAddress = (FloatingActionButton) findViewById(R.id.fabByPaymentAddress);
        fabByPaymentAddress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                multiple_actions.collapse();
                AddFriendPaymentAddressActivity.startForResult(FriendsActivity.this);
            }
        });

        FloatingActionButton fabFromContact = (FloatingActionButton) findViewById(R.id.fabFromContact);
        fabFromContact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                multiple_actions.collapse();

                BaseActivity activity = (BaseActivity) v.getContext();

                Intent intent = new Intent(activity, AddFriends.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                activity.startForResult(intent, 1, true);
            }
        });

        adapter.setOnItemClickListener(new FriendsListAdapter.OnItemClickListener() {

            @Override
            public void onItemSelected(FriendBean friendBean, int position) {

                Intent intent = new Intent();
                intent.putExtra(SELECTED_FRIEND, friendBean);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        llSearch = findViewById(R.id.llSearchView);

        ImageButton btnCloseSearch = (ImageButton) findViewById(R.id.btnCloseSearch);
        btnCloseSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                closeSearch();
            }
        });

        etSearch = (EditText) findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                onSearchText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void onSearchText(String s) {

        s = s.toLowerCase();

        if(StringUtils.isEmpty(s)) {

            adapter.setFriendBeans(friendBeans);
            return;
        }

        ArrayList<FriendBean> temp = new ArrayList<>();

        for(FriendBean friendBean : friendBeans) {

            String name = friendBean.getName().toLowerCase();

            if(name.startsWith(s)
                    || (s.length() > 2 && name.contains(s))){

                temp.add(friendBean);
            }
        }

        adapter.setFriendBeans(temp);
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

        friendBeans = new ArrayList<>();

        List<Model> models = new Select().from(FriendBean.class).execute();
        for(Model model : models) {

            friendBeans.add((FriendBean) model);
        }

        adapter.setFriendBeans(friendBeans);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_friends, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

       if (id == R.id.action_search) {

            search();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void search() {

        llSearch.setVisibility(View.VISIBLE);

        /*ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.hide();
        }*/

        toolbar.setVisibility(View.GONE);

        etSearch.requestFocus();

    }


    private void closeSearch() {

        llSearch.setVisibility(View.GONE);

        /*ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.show();
        }*/

        toolbar.setVisibility(View.VISIBLE);

        adapter.setFriendBeans(friendBeans);
        etSearch.setText(null);
    }

}
