package broids.com.paynow.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import broids.com.paynow.R;
import broids.com.paynow.adapter.SlideMenuAdapter;
import broids.com.paynow.entity.SlideMenuItem;
import broids.com.paynow.fragments.BaseFragment;
import broids.com.paynow.fragments.RecievePaymentFragment;
import broids.com.paynow.fragments.SendMoneyFragment;
import broids.com.paynow.fragments.SettingsFragment;
import broids.com.paynow.utils.AppPreferences;
import broids.com.paynow.utils.Base64ImageUtil;
import broids.com.paynow.utils.KeyboardUtils;
import broids.com.paynow.utils.Logger;
import broids.com.paynow.ws.entity.AadhaarKycBean;
import ui.DividerScroll;
import ui.RoundedImageView;

public class HomeActivity extends BaseActivity implements SlideMenuAdapter.SlideMenuSelectionListener {

    private DrawerLayout drawerLayout;

    private RoundedImageView ivProfile;

    private TextView tvProfileName;

    private RecyclerView rvDrawer;

    public View flContent;

    private CardView cvContentAndToolBar;

    private float lastTranslate;

    private Toolbar toolbar;

    private SlideMenuAdapter menuAdapter;

    private boolean isDashboard;

    private HashMap<String, BaseFragment> callingFragmentMap;
    private NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        NfcManager manager = (NfcManager) getApplicationContext().getSystemService(Context.NFC_SERVICE);
        nfcAdapter = manager.getDefaultAdapter();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    protected int getContentView() {

        return R.layout.activity_home;
    }

    @Override
    protected void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setVisibility(View.INVISIBLE);

        if (toolbar != null) {

            setSupportActionBar(toolbar);

            toolbar.setNavigationIcon(R.drawable.ic_header_icon_menu);

            setTitle(getString(R.string.home));
        }

        ivProfile = (RoundedImageView) findViewById(R.id.ivProfile);
        tvProfileName = (TextView) findViewById(R.id.tvName);

        flContent = findViewById(R.id.flContent);
        cvContentAndToolBar = (CardView) findViewById(R.id.cvContentAndToolBar);
        rvDrawer = (RecyclerView) findViewById(R.id.rvDrawer);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvDrawer.setLayoutManager(layoutManager);

        menuAdapter = new SlideMenuAdapter(this);
        menuAdapter.setSlideMenuSelectionListener(this);
        rvDrawer.setAdapter(menuAdapter);

        menuAdapter.populate();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {

            final float MAX_CARD_ELEVATION = 4;

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

                //                Decrease the Height and width of Framelayout when open drawer

                /*float moveFactor = (drawerView.getWidth() * slideOffset);

                float total = drawerView.getMeasuredWidth();

                total = (total == 0) ? 1 : total;

                float percent = moveFactor / total;

                //float x = ((int) moveFactor) == 0 ? 1 : 900 / moveFactor;
                float y = 1 - percent;

                final float MIN_SCALE = 0.85f;

                y = (y < MIN_SCALE) ? MIN_SCALE : y;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    cvContentAndToolBar.setTranslationX(moveFactor);

                    cvContentAndToolBar.setScaleX(y);
                    cvContentAndToolBar.setScaleY(y);

                } else {

                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    cvContentAndToolBar.startAnimation(anim);

                    ScaleAnimation scaleAnimation = new ScaleAnimation(0, 0, 0, y);
                    scaleAnimation.setDuration(0);
                    scaleAnimation.setFillAfter(true);
                    cvContentAndToolBar.startAnimation(scaleAnimation);

                    lastTranslate = moveFactor;
                }

                if (moveFactor == 0) {

                    cvContentAndToolBar.setCardElevation(0);

                } else {

                    cvContentAndToolBar.setCardElevation(MAX_CARD_ELEVATION * y);
                }*/
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                /*cvContentAndToolBar.setCardElevation(MAX_CARD_ELEVATION);*/
            }

            @Override
            public void onDrawerClosed(View drawerView) {

                /*cvContentAndToolBar.setCardElevation(0);*/
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        //Transperent the color of Drawer
        //drawerLayout.setScrimColor(Color.TRANSPARENT);

        DividerScroll.decorate(rvDrawer, R.drawable.divider1, true, true);
    }

    @Override
    public void setTextLabels() {

    }

    @Override
    public void setAppTheme() {
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean isNFCAvailable = getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC);

        if (isNFCAvailable) {
            if (nfcAdapter != null && nfcAdapter.isEnabled()) {
                //NFC Adapter exists and is enabled.
                if (intent.getType() != null && intent.getType().equals("application/paynow")) {
                    // Read the first record which contains the NFC data
                    Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
                    NdefRecord relayRecord = ((NdefMessage) rawMsgs[0]).getRecords()[0];
                    String nfcData = new String(relayRecord.getPayload());

                    Fragment fragment = getVisibleFragment();
                    if (fragment instanceof RecievePaymentFragment) {
                        RecievePaymentFragment recievePaymentFragment = (RecievePaymentFragment) fragment;
                        recievePaymentFragment.processNFC(nfcData);
                    } else if (fragment instanceof SendMoneyFragment) {
                        SendMoneyFragment sendMoneyFragment = (SendMoneyFragment) fragment;
                        sendMoneyFragment.processNFC(nfcData);
                    }
                }
            } else {
                Toast.makeText(HomeActivity.this, "Please start NFC", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(HomeActivity.this, "Device doesn't support NFC. Please use other Handle Discovery medium", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void setTitle(CharSequence title) {

        super.setTitle(title);
    }

    public void pushFragments(BaseFragment fragment, Fragment oldOne, boolean shouldAnimate, boolean shouldAdd, boolean isNaviClick) {

        try {
            KeyboardUtils.hideKeyboard(this);

            FragmentManager fragmentManager = getSupportFragmentManager();

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (isNaviClick) {

                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                isDashboard = false;
                /*FragmentTransaction transactionDashboard = fragmentManager.beginTransaction();
                SettingsFragment dashBoard = SettingsFragment.getInstance();
                transactionDashboard.replace(R.id.flContent, dashBoard);
                transactionDashboard.addToBackStack("");
                transactionDashboard.commit();*/
            }

            if (shouldAnimate) {

                int in = R.anim.enter;
                int out = R.anim.exit;
                int pop_in = R.anim.pop_enter;
                int pop_out = R.anim.pop_exit;

                /*int in = R.anim.exit;
                int out = R.anim.enter;
                int pop_in = R.anim.pop_exit;
                int pop_out = R.anim.pop_enter;*/

                fragmentTransaction.setCustomAnimations(in, out, pop_in, pop_out);

                if (oldOne != null) {

                    fragmentTransaction.hide(oldOne);
                }

            }

            fragmentTransaction.replace(R.id.flContent, fragment);

            if (shouldAdd) {
                fragmentTransaction.addToBackStack("");
            }

            fragmentTransaction.commit();

        } catch (Exception e) {
            Logger.e(e);
        }
    }

    public void popFragment() {

        FragmentManager manager = getSupportFragmentManager();

        int backCount = manager.getBackStackEntryCount();

        if (backCount != 0) {

            manager.popBackStack();

        } else {

            if (isDashboard) {

                finish();

            } else {

                dashBoard(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == android.R.id.home) {

            KeyboardUtils.hideKeyboard(this);

            openMenu();

            return true;

        } else {

            return super.onOptionsItemSelected(item);
        }
    }

    private void openMenu() {

        drawerLayout.openDrawer(Gravity.LEFT);
    }

    private boolean isDrawerOpen() {

        return drawerLayout != null && drawerLayout.isDrawerOpen(Gravity.LEFT);
    }

    public void closeMenu() {

        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    @Override
    protected void loadData() {

        AadhaarKycBean aadhaarKycBean = AadhaarKycBean.getKyc(this);

        if (aadhaarKycBean != null) {

            String name = aadhaarKycBean.getName();
            tvProfileName.setText(name);

            String bytes = aadhaarKycBean.getImageBase64();
            Bitmap bitmap = Base64ImageUtil.stringToBitmap(bytes);
            ivProfile.setImageBitmap(bitmap);
        }

        dashBoard(false);
        //sendPayment();
    }

    @Override
    protected void onResume() {
        super.onResume();

//        TestingActivity.testGetMerchantByUUID(this);
        //TestingActivity.transact(this);
    }

    @Override
    protected void onPause() {

        super.onPause();

    }

    public Fragment getVisibleFragment() {

        FragmentManager fragmentManager = this.getSupportFragmentManager();

        List<Fragment> fragments = fragmentManager.getFragments();

        for (Fragment fragment : fragments) {

            if (fragment != null && fragment.isVisible()) {

                return fragment;
            }
        }

        return null;
    }


    @Override
    public void onSlideMenuItemSelected(SlideMenuItem menuItem) {

        switch (menuItem.getId()) {

            case 0:
                transactionHistory(true);
                break;

            case 1:
                myProfile();
                break;

            case 2:
                signout();
                break;

            case 10:

                break;
        }
    }

    private void signout() {

        AppPreferences.clearAllPreferences(this);

        LoginActivity.start(this);
        finish();
    }

    private void myProfile() {

        ProfileActivity.start(this);
    }

    private void transactionHistory(boolean shouldAnimate) {

        TransactionHistoryActivity.start(this);
    }

    private void dashBoard(boolean shouldAnimate) {

        SendMoneyFragment sendMoneyFragment = SendMoneyFragment.getInstance();
        pushFragments(sendMoneyFragment, null, shouldAnimate, false, true);
        isDashboard = true;
        menuAdapter.setSelectedPosition(0);
    }

    private void settings() {

        SettingsFragment dashboardFragment = SettingsFragment.getInstance();
        pushFragments(dashboardFragment, null, true, false, true);
        menuAdapter.setSelectedPosition(1);

    }

    @Override
    public void onBackPressed() {

        if (isDrawerOpen()) {
            closeMenu();
            return;
        }

        popFragment();
    }

    public BaseFragment getCallingFragment(int requestCode) {

        if (callingFragmentMap != null
                && callingFragmentMap.containsKey(String.valueOf(requestCode))) {

            return callingFragmentMap.get(String.valueOf(requestCode));
        }

        return null;
    }

    public void removeCallingFragment(int requestCode) {

        if (callingFragmentMap != null
                && callingFragmentMap.containsKey(String.valueOf(requestCode))) {

            callingFragmentMap.remove(String.valueOf(requestCode));
        }
    }

    public void addCallingFragment(BaseFragment callingFragment, int requestCode) {

        if (callingFragmentMap == null) {

            callingFragmentMap = new HashMap<>();
        }

        this.callingFragmentMap.put(String.valueOf(requestCode), callingFragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }

    public static void start(BaseActivity activity) {

        Intent intent = new Intent(activity, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.start(intent, true);
    }
}
