package broids.com.paynow.activity;

import android.content.Intent;
import android.widget.EditText;

import broids.com.paynow.R;

/**
 * Created by NIKHIL on 3/18/2016.
 */
public class LinkPaymentAddressActivity extends BaseActivity {

    private EditText etPaymentAddress;

    @Override
    protected int getContentView() {

        return R.layout.activity_link_payment_address;
    }

    @Override
    protected void initViews() {

        etPaymentAddress = (EditText) findViewById(R.id.etPaymentAddress);
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

    }

    public static void start(BaseActivity activity) {

        Intent intent = new Intent(activity, LinkPaymentAddressActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.startForResult(intent, 1, true);
    }
}
