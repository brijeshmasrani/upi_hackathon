package broids.com.paynow.activity;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import broids.com.paynow.R;
import broids.com.paynow.entity.PaymentAddressBean;
import broids.com.paynow.utils.AppPreferences;
import broids.com.paynow.utils.FieldsValidator;
import broids.com.paynow.utils.Notify;
import broids.com.paynow.ws.AppWs;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;
import broids.com.paynow.ws.entity.LoginRequest;
import broids.com.paynow.ws.entity.LoginResponse;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout llDontHaveAccount;

    private TextView tvNext;

    private EditText etEmail;

    private EditText etPassword;

    @Override
    protected int getContentView() {

        return R.layout.activity_login;
    }

    @Override
    protected void initViews() {

        llDontHaveAccount = (LinearLayout) findViewById(R.id.llDontHaveAccount);
        llDontHaveAccount.setOnClickListener(this);

        tvNext = (TextView) findViewById(R.id.tvNext);
        tvNext.setOnClickListener(this);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        //etEmail.setError("This is error");
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.llDontHaveAccount :
                register();
                break;

            case R.id.tvNext :
                next();
                break;

        }
    }

    private void next() {

        if (!checkValidations()) {

            return;
        }

        LoginRequest request = new LoginRequest();
        /*request.setEmail("test@gmail.com");
        request.setPassword("test");*/

        request.setEmail(etEmail.getText().toString());
        request.setPassword(etPassword.getText().toString());

        AppWs.login(request, this, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                if (baseResponse instanceof LoginResponse) {

                    moveForward();

                    LoginResponse response = (LoginResponse) baseResponse;
                    AppPreferences.setString(AppPreferences.USERID, response.getUserId(), LoginActivity.this);
                }
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

            }
        });
    }

    private boolean checkValidations() {

        return FieldsValidator.validateEmail(etEmail)
                && FieldsValidator.validatePassword(etPassword);
    }

    private void moveForward() {

        ArrayList<String> addresses = PaymentAddressBean.getAllAddresses();
        boolean isFirstTime = addresses.size() == 0;
        if (isFirstTime) {

            CreatePaymentAddressActivity.startForResult(this);

        } else {

            home();
        }
    }

    private void home() {

        HomeActivity.start(this);
        finish();
    }

    private void register() {

        AadhaarVerificationActivity.start(this);
        finish();
    }

    private void profile() {

        ProfileActivity.startFirstTime(this);
        finish();
    }

    /*private void otp() {

        Intent intent = new Intent(this, OTPActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startForResult(intent, true);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {

            if(requestCode == CreatePaymentAddressActivity.REQUEST_CODE) {

                profile();
            }
        }
    }

    public static void start(BaseActivity activity) {

        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.start(intent, true);
    }
}
