package broids.com.paynow.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aadhaarconnect.bridge.capture.model.auth.AuthCaptureRequest;
import com.aadhaarconnect.bridge.capture.model.common.ConsentType;
import com.aadhaarconnect.bridge.capture.model.common.Location;
import com.aadhaarconnect.bridge.capture.model.common.LocationType;
import com.aadhaarconnect.bridge.capture.model.common.request.CertificateType;
import com.aadhaarconnect.bridge.capture.model.common.request.Modality;
import com.aadhaarconnect.bridge.capture.model.kyc.KycCaptureRequest;
import com.aadhaarconnect.bridge.capture.model.otp.OtpCaptureData;
import com.aadhaarconnect.bridge.capture.model.otp.OtpCaptureRequest;
import com.aadhaarconnect.bridge.capture.model.otp.OtpChannel;
import com.aadhaarconnect.bridge.gateway.model.OtpResponse;
import com.google.gson.Gson;

import broids.com.paynow.R;
import broids.com.paynow.async.AadhaarOTPAuthAsyncTask;
import broids.com.paynow.async.AadhaarOTPKycAsyncTask;
import broids.com.paynow.onTaskCompleted;
import broids.com.paynow.utils.Config;
import broids.com.paynow.utils.KeyboardUtils;

public class OTPActivity extends BaseActivity implements View.OnClickListener, onTaskCompleted {

    public static final int REQUEST_CODE = 5011;
    public static final int AADHAAR_OTP_AUTH_REQUEST = 1000;

    private TextView tvNext;

    private TextView tvReset;

    private EditText et1;
    private EditText et2;
    private EditText et3;
    private EditText et4;
    private EditText et5;
    private EditText et6;

    protected ProgressDialog mDialog;
    private String mAadhaarNumber;

    @Override
    protected int getContentView() {

        return R.layout.activity_otp;
    }

    @Override
    protected void initViews() {

        tvNext = (TextView) findViewById(R.id.tvNext);

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);
        et5 = (EditText) findViewById(R.id.et5);
        et6 = (EditText) findViewById(R.id.et6);

        reset();

        et1.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    et1.setEnabled(false);
                    et2.requestFocus();
                    et2.setEnabled(true);
                    KeyboardUtils.showKeyboard(et2);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et2.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    et2.setEnabled(false);
                    et3.requestFocus();
                    et3.setEnabled(true);
                    KeyboardUtils.showKeyboard(et3);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et3.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    et3.setEnabled(false);
                    et4.requestFocus();
                    et4.setEnabled(true);
                    KeyboardUtils.showKeyboard(et4);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et4.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    et4.setEnabled(false);
                    et5.requestFocus();
                    et5.setEnabled(true);
                    KeyboardUtils.showKeyboard(et5);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et5.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    et5.setEnabled(false);
                    et6.requestFocus();
                    et6.setEnabled(true);
                    KeyboardUtils.showKeyboard(et6);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et6.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    et6.setEnabled(false);
                    done();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        tvReset = (TextView) findViewById(R.id.tvReset);
        tvReset.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                reset();
            }
        });
    }

    private void reset() {

        et1.setEnabled(true);

        et1.requestFocus();
        et2.setEnabled(false);
        et3.setEnabled(false);
        et4.setEnabled(false);
        et5.setEnabled(false);
        et6.setEnabled(false);

        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        et5.setText("");
        et6.setText("");

        KeyboardUtils.showKeyboard(et1);

    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

        Bundle bundle = getIntent().getExtras();
        mAadhaarNumber = bundle.getString("aadhaarNumber");

        sendOTPRequest(mAadhaarNumber);
    }

    private void sendOTPRequest(String aadhaarNumber) {
        mDialog = new ProgressDialog(OTPActivity.this);
        mDialog.setCancelable(true);
        mDialog.setMessage("Please Wait.. Requesting OTP (One Time Password)");
        mDialog.show();

        Intent intent = new Intent("com.aadhaarconnect.bridge.action.OTPCAPTURE");

        OtpCaptureRequest otpRequest = new OtpCaptureRequest();
        otpRequest.setAadhaar(aadhaarNumber);
        otpRequest.setCertificateType(CertificateType.preprod);
        otpRequest.setChannel(OtpChannel.SMS);

        Location loc = new Location();
        loc.setType(LocationType.pincode);
        loc.setPincode("560076");

        otpRequest.setLocation(loc);
        intent.putExtra("REQUEST", new Gson().toJson(otpRequest));

        try {
            startActivityForResult(intent, AADHAAR_OTP_AUTH_REQUEST);
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {

            String responseStr = data.getStringExtra("RESPONSE");
            final OtpCaptureData response = new Gson().fromJson(responseStr,
                    OtpCaptureData.class);

            AadhaarOTPAuthAsyncTask authAsyncTask = new AadhaarOTPAuthAsyncTask(
                    OTPActivity.this, response, this);
            authAsyncTask.execute(Config.BASE_URL + "/otp");
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tvNext:
                done();
                break;

        }
    }

    private void done() {

        String otp = et1.getText().toString() + et2.getText().toString() +
                et3.getText().toString() + et4.getText().toString() +
                et5.getText().toString() + et6.getText().toString();

        com.aadhaarconnect.bridge.capture.model.common.Location loc = new com.aadhaarconnect.bridge.capture.model.common.Location();
        loc.setType(com.aadhaarconnect.bridge.capture.model.common.LocationType.pincode);
        loc.setPincode("560001");

        AuthCaptureRequest authCaptureRequest = new AuthCaptureRequest();
        authCaptureRequest.setAadhaar(mAadhaarNumber);
        authCaptureRequest.setLocation(loc);
        authCaptureRequest.setModality(Modality.otp);
        authCaptureRequest.setCertificateType(CertificateType.preprod);
        authCaptureRequest.setOtp(otp);

        KycCaptureRequest request = new KycCaptureRequest();
        request.setConsent(ConsentType.Y);
        request.setAuthCaptureRequest(authCaptureRequest);

        AadhaarOTPKycAsyncTask authAsyncTask = new AadhaarOTPKycAsyncTask(OTPActivity.this, new AadhaarOTPKycAsyncTask.OTPKycListener() {

            @Override
            public void onAuthenticationSuccess(boolean isSuccess) {

                //Tmporary bypass auth
                //if(isSuccess){

                    /*Intent intent = new Intent();
                    intent.setClass(OTPActivity.this, CreatePaymentAddressActivity.class);
                    startActivity(intent);*/

                setResult(RESULT_OK);
                finish();
                //}
            }
        });

        authAsyncTask.execute(request);

    }

//    public static void start(BaseActivity baseActivity, String aadhaarNumber) {
//
//        Bundle bundle = new Bundle();
//        bundle.putString("aadhaarNumber", aadhaarNumber);
//        baseActivity.start(OTPActivity.class, bundle, true);
//    }

    public static void startForResult(BaseActivity baseActivity, String aadhaarNumber) {

        Bundle bundle = new Bundle();
        bundle.putString("aadhaarNumber", aadhaarNumber);

        Intent intent = new Intent(baseActivity, OTPActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        baseActivity.startForResult(intent, bundle, REQUEST_CODE, true);
    }

    @Override
    public void onTaskCompleted(OtpResponse response) {
        if ((this.mDialog != null) && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }

        try {
            if (null != response && response.isSuccess()) {
                Toast.makeText(OTPActivity.this, "Please Enter the OTP", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(OTPActivity.this, "Error generating OTP", Toast.LENGTH_LONG).show();
        }
    }
}
