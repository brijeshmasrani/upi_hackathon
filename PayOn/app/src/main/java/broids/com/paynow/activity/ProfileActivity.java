package broids.com.paynow.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

import broids.com.paynow.R;
import broids.com.paynow.adapter.MyPaymentAddressListAdapter;
import broids.com.paynow.entity.PaymentAddressBean;
import broids.com.paynow.utils.Base64ImageUtil;
import broids.com.paynow.ws.entity.AadhaarKycBean;
import ui.LinearLayoutManager;
import ui.RoundedImageView;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {

    public static final String IS_FIRST_TME = "is_first_time";

    private RecyclerView rvPaymentAddresses;

    private MyPaymentAddressListAdapter adapter;

    private FloatingActionButton fabLinkPaymentAddress;

    private FloatingActionButton fabCreateNewPaymentAddress;

    private Toolbar toolbar;

    private RoundedImageView ivProfile;

    private TextView tvEmail;

    private TextView tvPhone;

    private boolean isFirstTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        readArguments();

        super.onCreate(savedInstanceState);

    }

    private void readArguments() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFirstTime = bundle.getBoolean(IS_FIRST_TME, false);
        }
    }

    @Override
    protected int getContentView() {

        return R.layout.activity_profile;
    }

    @Override
    protected void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setVisibility(View.INVISIBLE);

        if (toolbar != null) {

            setSupportActionBar(toolbar);

            if (!isFirstTime) {

                toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

                toolbar.setNavigationOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        finish();
                    }
                });
            }
        }

        final FloatingActionsMenu multiple_actions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);

        fabLinkPaymentAddress = (FloatingActionButton) findViewById(R.id.fabLinkPaymentAddress);
        fabLinkPaymentAddress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                multiple_actions.collapse();
                LinkPaymentAddressActivity.start(ProfileActivity.this);
            }
        });

        fabCreateNewPaymentAddress = (FloatingActionButton) findViewById(R.id.fabCreateNewPaymentAddress);
        fabCreateNewPaymentAddress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                multiple_actions.collapse();
                CreatePaymentAddressActivity.startForResult(ProfileActivity.this);
            }
        });

        rvPaymentAddresses = (RecyclerView) findViewById(R.id.rvPaymentAddresses);

        adapter = new MyPaymentAddressListAdapter(this);
        rvPaymentAddresses.setAdapter(adapter);

        adapter.setOnItemClickListener(new MyPaymentAddressListAdapter.OnItemClickListener() {

            @Override
            public void onItemSelected(PaymentAddressBean paymentAddressBean, int position) {

            }

            @Override
            public void onAddButtonClicked(PaymentAddressBean paymentAddressBean, int position) {

            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvPaymentAddresses.setLayoutManager(layoutManager);

        ivProfile = (RoundedImageView) findViewById(R.id.ivProfile);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvPhone = (TextView) findViewById(R.id.tvPhone);
    }

    private void addPaymentAddress() {


    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

        showProgressDialog();

        showAadhaarKyc();

        ArrayList<PaymentAddressBean> paymentAddressBeans = new ArrayList<>();

        paymentAddressBeans = PaymentAddressBean.getAll();

        adapter.setPaymentAddressBeans(paymentAddressBeans);

        dismissProgressDialog();
    }

    private void showAadhaarKyc() {

        AadhaarKycBean aadhaarKycBean = AadhaarKycBean.getKyc(this);

        if (aadhaarKycBean != null) {

            String name = aadhaarKycBean.getName();
            setTitle(name);

            String bytes = aadhaarKycBean.getImageBase64();
            Bitmap bitmap = Base64ImageUtil.stringToBitmap(bytes);
            ivProfile.setImageBitmap(bitmap);

            String email = aadhaarKycBean.getEmail();
            tvEmail.setText(email);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            /*case R.id.tvDoThisLater:
                home();
                break;*/

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (isFirstTime) {

            getMenuInflater().inflate(R.menu.menu_done, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_done) {

            done();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void done() {

        HomeActivity.start(this);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        loadData();
    }

    public static void startFirstTime(BaseActivity activity) {

        Intent intent = new Intent(activity, ProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(IS_FIRST_TME, true);

        activity.start(intent, true);
    }

    public static void start(BaseActivity activity) {

        Intent intent = new Intent(activity, ProfileActivity.class);
        activity.start(intent, true);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
