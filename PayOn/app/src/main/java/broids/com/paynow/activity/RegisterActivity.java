package broids.com.paynow.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.Settings;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import broids.com.paynow.R;
import broids.com.paynow.utils.Base64ImageUtil;
import broids.com.paynow.utils.FieldsValidator;
import broids.com.paynow.utils.Notify;
import broids.com.paynow.ws.AppWs;
import broids.com.paynow.ws.entity.AadhaarKycBean;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;
import broids.com.paynow.ws.entity.RegisterRequest;
import broids.com.paynow.ws.entity.RegisterResponse;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvNext;
    private TextView tvName;

    private EditText etEmail;
    private EditText etPassword;
    private EditText etRePassword;

    @Override
    protected int getContentView() {

        return R.layout.activity_register;
    }

    @Override
    protected void initViews() {

        tvNext = (TextView) findViewById(R.id.tvNext);
        tvNext.setOnClickListener(this);

        tvName = (TextView) findViewById(R.id.tvName);

        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etRePassword = (EditText) findViewById(R.id.etRePassword);
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

        AadhaarKycBean aadhaarKycBean = AadhaarKycBean.getKyc(this);

        if (aadhaarKycBean != null) {

            String name = aadhaarKycBean.getName();
            tvName.setText(name);

            /*String bytes = aadhaarKycBean.getImageBase64();
            Bitmap bitmap = Base64ImageUtil.stringToBitmap(bytes);
            iv.setImageBitmap(bitmap);*/

            String email = aadhaarKycBean.getEmail();
            etEmail.setText(email);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tvNext:
                next();
                break;

        }
    }

    private void next() {

        if (!checkValidations()) {

            return;
        }

        String androidDeviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        AadhaarKycBean aadhaarKycBean = AadhaarKycBean.getKyc(this);

        RegisterRequest request = new RegisterRequest();
        request.setEmail(etEmail.getText().toString());
        request.setPassword(etPassword.getText().toString());
        request.setAadharNumber(aadhaarKycBean.getAadhaar());
        request.setDeviceId(androidDeviceId);
        request.setFirstName(aadhaarKycBean.getName());
        request.setLastName("");
        request.setPhoneNumber(aadhaarKycBean.getPhone());

        showProgressDialog();
        AppWs.register(request, this, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                dismissProgressDialog();

                if (baseResponse instanceof RegisterResponse) {

                    RegisterResponse response = (RegisterResponse) baseResponse;
                    login();
                }
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

                dismissProgressDialog();
            }
        });
    }

    private boolean checkValidations() {
        return FieldsValidator.validateEmail(etEmail)
                && FieldsValidator.validatePassword(etPassword)
                && FieldsValidator.matchPasswords(etPassword, etRePassword);
    }

    private void login() {

        LoginActivity.start(this);
        finish();
    }

    private void home() {

        HomeActivity.start(this);
        finish();
    }

    public static void start(BaseActivity activity) {

        Intent intent = new Intent(activity, RegisterActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.start(intent, true);
    }

}
