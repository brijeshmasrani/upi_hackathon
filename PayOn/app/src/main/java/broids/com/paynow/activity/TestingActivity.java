package broids.com.paynow.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import broids.com.paynow.R;
import broids.com.paynow.utils.Base64ImageUtil;
import broids.com.paynow.utils.Notify;
import broids.com.paynow.ws.AppWs;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;
import broids.com.paynow.ws.entity.GetMerchantByUUIDRequest;
import broids.com.paynow.ws.entity.GetMerchantByUUIDResponse;
import broids.com.paynow.ws.entity.LoginRequest;
import broids.com.paynow.ws.entity.LoginResponse;
import broids.com.paynow.ws.entity.RegisterRequest;
import broids.com.paynow.ws.entity.RegisterResponse;
import broids.com.paynow.ws.entity.TransactResponse;
import broids.com.paynow.entity.TransactionBean;

public class TestingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView iv = (ImageView) findViewById(R.id.iv);
        byte[] bytes = new byte[0];
        try {

            InputStream is = getAssets().open("sample_base_64.txt");
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            bytes = buffer.toByteArray();;

            Bitmap bitmap = Base64ImageUtil.bytesToBitmap(bytes);
            iv.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*ImageView iv = (ImageView) findViewById(R.id.iv);

        try {

            InputStream is = getAssets().open("sample_base_64.txt");
            StringBuilder buf = new StringBuilder();
            BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String str;

            while ((str = in.readLine()) != null) {
                buf.append(str);
            }

            in.close();

            Bitmap bitmap = Base64ImageUtil.stringToBitmap(buf.toString());
            iv.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //testLogin();
                testRegister();

                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

    }

    private void testLogin() {

        LoginRequest request = new LoginRequest();
        request.setEmail("test@gmail.com");
        request.setPassword("test");

        AppWs.login(request, this, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                if (baseResponse instanceof LoginResponse) {

                    LoginResponse response = (LoginResponse) baseResponse;
                    Notify.toast(response.getResponse(), TestingActivity.this);
                }
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

            }
        });
    }

    private void testRegister() {

        RegisterRequest request = new RegisterRequest();
        request.setEmail("test@gmail.com");
        request.setPassword("test");
        request.setAadharNumber("123123");
        request.setDeviceId("12222");
        request.setFirstName("Tester");
        request.setLastName("Us");
        request.setPhoneNumber("+919898878765");

        AppWs.register(request, this, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                if (baseResponse instanceof RegisterResponse) {

                    RegisterResponse response = (RegisterResponse) baseResponse;
                    Notify.toast(response.getResponse(), TestingActivity.this);
                }
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public static void testGetMerchantByUUID(final Context context, String beaconKey) {

        GetMerchantByUUIDRequest request = new GetMerchantByUUIDRequest();
        request.setMerchantUid(beaconKey);

        AppWs.getMerchantDetailsByUUID(request, context, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                if (baseResponse instanceof GetMerchantByUUIDResponse) {

                    GetMerchantByUUIDResponse merchantByUUIDResponse = (GetMerchantByUUIDResponse) baseResponse;

                    Notify.toast(merchantByUUIDResponse.getMerchantBean().getMerchantName(), context);
                }
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

            }
        });

    }


    public static void transact(final Context context) {

        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setAmount("50");
        transactionBean.setDescription("From android");
        transactionBean.setPayeeId("payeeId");
        transactionBean.setPayerId("payerId");
        transactionBean.setPayeeVpa("payee_vpa");
        transactionBean.setPayerVpa("payer_vpa");
        transactionBean.setPayerName("Payer Name");
        transactionBean.setPayeeName("Payee Name");
        transactionBean.setStatus(TransactionBean.TRX_STATUS_PENDING);
        transactionBean.setUpiTransactionId("upiXnId");

        AppWs.transact(transactionBean, context, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                if (baseResponse instanceof TransactResponse) {


                }
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
