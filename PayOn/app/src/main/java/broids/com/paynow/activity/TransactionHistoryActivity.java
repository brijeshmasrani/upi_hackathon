package broids.com.paynow.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.activeandroid.Model;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

import broids.com.paynow.R;
import broids.com.paynow.adapter.TransactionHistoryAdapter;
import broids.com.paynow.entity.FriendBean;
import broids.com.paynow.entity.TransactionBean;
import broids.com.paynow.utils.AppPreferences;
import broids.com.paynow.utils.StringUtils;
import broids.com.paynow.ws.AppWs;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;
import broids.com.paynow.ws.entity.TransactionHistoryRequest;
import broids.com.paynow.ws.entity.TransactionHistoryResponse;
import ui.DividerScroll;

public class TransactionHistoryActivity extends BaseActivity {

    public static final String SELECTED_FRIEND = "selected_friend";

    public static final int REQUEST_CODE = 1110;

    private RecyclerView rvHistory;

    private TransactionHistoryAdapter adapter;

    private Toolbar toolbar;

    private View llSearch;

    private EditText etSearch;

    private ArrayList<TransactionBean> transactionBeans = new ArrayList<>();

    @Override
    protected int getContentView() {

        return R.layout.activity_transaction_history;
    }

    @Override
    protected void initViews() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setVisibility(View.INVISIBLE);

        if (toolbar != null) {

            setSupportActionBar(toolbar);

            toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

            toolbar.setNavigationOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    finish();
                }
            });

            setTitle(getString(R.string.transaction_hitory));
        }

        rvHistory = (RecyclerView) findViewById(R.id.rvHistory);

        adapter = new TransactionHistoryAdapter(this);
        rvHistory.setAdapter(adapter);

        rvHistory.setLayoutManager(new LinearLayoutManager(this));
        DividerScroll.decorate(rvHistory, R.drawable.divider1, false, true);

        adapter.setOnItemClickListener(new TransactionHistoryAdapter.OnItemClickListener() {

            @Override
            public void onItemSelected(TransactionBean friendBean, int position) {


            }
        });

        llSearch = findViewById(R.id.llSearchView);

        ImageButton btnCloseSearch = (ImageButton) findViewById(R.id.btnCloseSearch);
        btnCloseSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                closeSearch();
            }
        });

        etSearch = (EditText) findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                onSearchText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void onSearchText(String s) {

        s = s.toLowerCase();

        if(StringUtils.isEmpty(s)) {

            adapter.setTransactionBeans(transactionBeans);
            return;
        }

        ArrayList<TransactionBean> temp = new ArrayList<>();

        for(TransactionBean transactionBean : transactionBeans) {

            String payeeName = transactionBean.getPayeeName().toLowerCase();
            String payerName = transactionBean.getPayerName().toLowerCase();

            if(payeeName.startsWith(s)
                    || (s.length() > 2 && payeeName.contains(s))){

                temp.add(transactionBean);
            }

            if(payerName.startsWith(s)
                    || (s.length() > 2 && payerName.contains(s))){

                temp.add(transactionBean);
            }
        }

        adapter.setTransactionBeans(temp);
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    public void setAppTheme() {

    }

    @Override
    protected void loadData() {

        List<Model> models = new Select().from(TransactionBean.class).execute();
        for(Model model : models) {

            transactionBeans.add((TransactionBean) model);
        }

        String userId = AppPreferences.getString(AppPreferences.USERID, "", this);

        TransactionHistoryRequest request = new TransactionHistoryRequest();
        request.setUserId(userId);
        request.setUserId("1");

        showProgressDialog();
        AppWs.getTransactionHistory(request, this, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                dismissProgressDialog();

                if (baseResponse instanceof TransactionHistoryResponse) {

                    TransactionHistoryResponse response = (TransactionHistoryResponse) baseResponse;
                    ArrayList<TransactionBean> history = response.getHistory();

                    transactionBeans.addAll(history);
                }

                adapter.setTransactionBeans(transactionBeans);
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

                adapter.setTransactionBeans(transactionBeans);

                dismissProgressDialog();
            }
        });

        adapter.setTransactionBeans(transactionBeans);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_friends, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

       if (id == R.id.action_search) {

            search();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void search() {

        llSearch.setVisibility(View.VISIBLE);

        /*ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.hide();
        }*/

        toolbar.setVisibility(View.GONE);

        etSearch.requestFocus();

    }


    public static void start(BaseActivity activity) {

        Intent intent = new Intent(activity, TransactionHistoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.start(intent, true);
    }

    private void closeSearch() {

        llSearch.setVisibility(View.GONE);

        /*ActionBar actionBar = getActionBar();
        if(actionBar != null) {
            actionBar.show();
        }*/

        toolbar.setVisibility(View.VISIBLE);

        adapter.setTransactionBeans(transactionBeans);
        etSearch.setText(null);
    }
}
