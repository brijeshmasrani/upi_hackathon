package broids.com.paynow.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.mobstac.beaconstac.models.MSBeacon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import broids.com.paynow.R;
import broids.com.paynow.activity.TestingActivity;
import broids.com.paynow.utils.Notify;
import broids.com.paynow.ws.AppWs;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;
import broids.com.paynow.ws.entity.GetMerchantByUUIDRequest;
import broids.com.paynow.ws.entity.GetMerchantByUUIDResponse;


public class BeaconAdapter extends BaseAdapter {
    private ArrayList<MSBeacon> beacons;
    private Context ctx;
    private LayoutInflater myInflator;

    public BeaconAdapter(ArrayList<MSBeacon> arr, Context c) {
        super();
        beacons = arr;
        ctx = c;
        myInflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addBeacon(MSBeacon beacon) {
        if(!beacons.contains(beacon)) {
            beacons.add(beacon);
        }
    }

    public void removeBeacon(MSBeacon beacon) {
        if(beacons.contains(beacon)) {
            beacons.remove(beacon);
        }
    }

    public void clear() {
        beacons.clear();
    }

    @Override
    public int getCount() {
        return beacons.size();
    }

    @Override
    public MSBeacon getItem(int position) {
        return beacons.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void notifyDataSetChanged() {
        Collections.sort(beacons, new Comparator<MSBeacon>() {
            @Override
            public int compare(MSBeacon lhs, MSBeacon rhs) {
                if (lhs.getLatestRSSI() > rhs.getLatestRSSI())
                    return -1;
                else if (lhs.getLatestRSSI() < rhs.getLatestRSSI())
                    return 1;
                else
                    return 0;
            }
        });

        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (view == null) {
            view = myInflator.inflate(R.layout.dialog_ripple_effect, parent, false);
        }
        final MSBeacon beacon = beacons.get(position);

        final TextView name = (TextView) view.findViewById(R.id.text_merchant);
        final RippleView rippleView = (RippleView) view.findViewById(R.id.rl);
        final TextView title = (TextView) view.findViewById(R.id.tvDialogTitle);


        GetMerchantByUUIDRequest request = new GetMerchantByUUIDRequest();
        request.setMerchantUid("f94dbb23-2266-7822-3782-57beac0952ac:36098:6549");

        AppWs.getMerchantDetailsByUUID(request, ctx, new AppWs.WsListener() {

            @Override
            public void onResponseSuccess(BaseResponse baseResponse) {

                if (baseResponse instanceof GetMerchantByUUIDResponse) {

                    GetMerchantByUUIDResponse merchantByUUIDResponse = (GetMerchantByUUIDResponse) baseResponse;
                    name.setText(merchantByUUIDResponse.getMerchantBean().getMerchantName());
                    name.setVisibility(View.VISIBLE);
                    rippleView.setVisibility(View.GONE);
                    title.setText(R.string.text_beacon_found_title);

                    name.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TextView textView  = (TextView)view;
                            Toast.makeText(ctx, "Clicked on " +textView.getText(),
                                    Toast.LENGTH_LONG).show();



                        }
                    });
                }
            }

            @Override
            public void notifyResponseFailed(String message, BaseRequest request) {

            }
        });

       /* TextView key = (TextView) view.findViewById(R.id.device_address);
        key.setText("Major: " + beacon.getMajor() + "\t\t\t Minor: " + beacon.getMinor() +
            " \t\t\t  Mean RSSI: " + beacon.getMeanRSSI());*/

       /* if (beacon.getIsCampedOn()) {
            view.setBackgroundResource(android.R.color.holo_green_light);
        } else {
            view.setBackgroundResource(android.R.color.background_light);
        }*/

        return view;
    }
}
