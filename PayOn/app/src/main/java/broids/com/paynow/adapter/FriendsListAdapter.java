package broids.com.paynow.adapter;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import broids.com.paynow.R;
import broids.com.paynow.activity.BaseActivity;
import broids.com.paynow.entity.FriendBean;

public class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.ViewHolder> {

    private ArrayList<FriendBean> friendBeans = new ArrayList<>();

    private BaseActivity activity;

    private LayoutInflater inflater;

    private OnItemClickListener onItemClickListener;

    public FriendsListAdapter(BaseActivity activity) {

        this.activity = activity;

        inflater = LayoutInflater.from(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.row_friend, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final FriendBean bean = friendBeans.get(position);

        holder.tvEmployee.setText(bean.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (onItemClickListener != null) {

                    onItemClickListener.onItemSelected(bean, position);
                }

                notifyDataSetChanged();
            }
        });

        holder.ivStatus.setVisibility(View.GONE);
        holder.btnInvite.setVisibility(View.GONE);

        Uri photoUri = bean.getPhotoUri();
        if(photoUri != null) {

            holder.ivProfile.setImageURI(photoUri);
        } else {

            Bitmap bitmap = Bitmap.createBitmap(20, 20, Bitmap.Config.ARGB_4444);
            bitmap.eraseColor(bean.getColorCode());
            holder.ivProfile.setImageBitmap(bitmap);
        }
    }

    @Override
    public int getItemCount() {

        return friendBeans.size();
    }

    public OnItemClickListener getOnItemClickListener() {

        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {

        this.onItemClickListener = onItemClickListener;
    }

    public ArrayList<FriendBean> getFriendBeans() {

        return friendBeans;
    }

    public void setFriendBeans(ArrayList<FriendBean> alEmployee) {

        if (alEmployee == null) {

            alEmployee = new ArrayList<>();
        }

        this.friendBeans = alEmployee;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View itemView;

        public TextView tvEmployee;

        public ImageView ivStatus;

        public ImageView ivProfile;

        public Button btnInvite;

        public ViewHolder(View itemView) {

            super(itemView);

            this.itemView = itemView;

            tvEmployee = (TextView) itemView.findViewById(R.id.tvName);
            ivStatus = (ImageView) itemView.findViewById(R.id.ivStatus);
            ivProfile = (ImageView) itemView.findViewById(R.id.ivProfile);
            btnInvite = (Button) itemView.findViewById(R.id.btnInvite);
        }
    }

    public interface OnItemClickListener {

        void onItemSelected(FriendBean friendBean, int position);
    }
}
