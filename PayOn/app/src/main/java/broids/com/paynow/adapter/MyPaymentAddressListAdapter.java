package broids.com.paynow.adapter;

import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import broids.com.paynow.R;
import broids.com.paynow.activity.BaseActivity;
import broids.com.paynow.activity.CreatePaymentAddressActivity;
import broids.com.paynow.entity.BankAccountBean;
import broids.com.paynow.entity.PaymentAddressBean;
import broids.com.paynow.utils.KeyboardUtils;
import broids.com.paynow.utils.Notify;

public class MyPaymentAddressListAdapter extends RecyclerView.Adapter<MyPaymentAddressListAdapter.ViewHolder> {

    private ArrayList<PaymentAddressBean> paymentAddressBeans = new ArrayList<>();

    private BaseActivity activity;

    private LayoutInflater inflater;

    private OnItemClickListener onItemClickListener;

    public MyPaymentAddressListAdapter(BaseActivity activity) {

        this.activity = activity;

        inflater = LayoutInflater.from(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.row_payment_address, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final PaymentAddressBean bean = paymentAddressBeans.get(position);

        holder.tvPaymentAddress.setText(bean.getPaymentAddress());

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (onItemClickListener != null) {

                    onItemClickListener.onItemSelected(bean, position);
                }

                notifyDataSetChanged();
            }
        });

        holder.tvPaymentAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopupMenu(v, bean);
            }
        });

        holder.llContent.removeAllViews();

        ArrayList<BankAccountBean> bankAccounts = bean.getBankAccounts();

        for(BankAccountBean accountBean : bankAccounts) {

            if(accountBean != null) {

                View view = inflater.inflate(R.layout.row_bank_acc_detail, null);

                TextView tvAccNo = (TextView) view.findViewById(R.id.tvAccountNumber);
                TextView tvIFSC = (TextView) view.findViewById(R.id.tvifsc);

                tvAccNo.setText(accountBean.getAccountNumber());
                tvIFSC.setText(accountBean.getIfscCode());

                View divider = inflater.inflate(R.layout.layout_divider, null);

                holder.llContent.addView(view);
                holder.llContent.addView(divider);

            }
        }
    }

    private void showPopupMenu(View view, final PaymentAddressBean bean) {

        final PopupMenu popupMenu = new PopupMenu(activity, view , Gravity.RIGHT);
        final Menu menu = popupMenu.getMenu();

        popupMenu.getMenuInflater().inflate(R.menu.menu_payment_address, menu);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                int id = item.getItemId();

                if (id == R.id.action_make_preferred_account) {

                    makPreferredAccount(bean);
                    return true;

                } else if (id == R.id.action_delete_payment_address) {

                    delete(bean);
                    return true;

                } else if (id == R.id.action_link_to_bank_account) {

                    linkBankAccount(bean);
                    return true;
                }

                return false;
            }
        });

        popupMenu.show();
    }

    private void linkBankAccount(PaymentAddressBean paymentAddressBean) {

        CreatePaymentAddressActivity.startForResult(paymentAddressBean.getPaymentAddress(), activity);
    }

    private void delete(final PaymentAddressBean paymentAddressBean) {

        String message = activity.getString(R.string.delete_payment_address) + "\""+paymentAddressBean.getPaymentAddress()+"\"?";

        Notify.dialogYesNo(message, activity, new Notify.OnDialogDismiss() {
            @Override
            public void onDialogDismiss(boolean isPositive) {

                if(isPositive) {

                    String address = paymentAddressBean.getPaymentAddress();
                    BankAccountBean.deleteLinkedAccounts(address);

                    paymentAddressBean.delete();

                    paymentAddressBeans.remove(paymentAddressBean);
                    notifyDataSetChanged();
                }
            }
        });

    }

    private void makPreferredAccount(PaymentAddressBean paymentAddressBean) {


    }

    @Override
    public int getItemCount() {

        return paymentAddressBeans.size();
    }

    public OnItemClickListener getOnItemClickListener() {

        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {

        this.onItemClickListener = onItemClickListener;
    }

    public ArrayList<PaymentAddressBean> getPaymentAddressBeans() {

        return paymentAddressBeans;
    }

    public void setPaymentAddressBeans(ArrayList<PaymentAddressBean> alEmployee) {

        if (alEmployee == null) {

            alEmployee = new ArrayList<>();
        }

        this.paymentAddressBeans = alEmployee;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View itemView;

        public LinearLayout llContent;

        public TextView tvPaymentAddress;

        public ViewHolder(View itemView) {

            super(itemView);

            this.itemView = itemView;

            tvPaymentAddress = (TextView) itemView.findViewById(R.id.tvPaymentAddress);
            llContent = (LinearLayout) itemView.findViewById(R.id.llContent);
        }
    }

    public interface OnItemClickListener {

        void onItemSelected(PaymentAddressBean paymentAddressBean, int position);
        void onAddButtonClicked(PaymentAddressBean paymentAddressBean, int position);
    }
}
