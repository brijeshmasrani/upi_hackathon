package broids.com.paynow.adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;


import java.util.ArrayList;

import broids.com.paynow.R;
import broids.com.paynow.activity.HomeActivity;
import broids.com.paynow.entity.SlideMenuItem;
import broids.com.paynow.utils.StringUtils;


public class SlideMenuAdapter extends RecyclerView.Adapter<SlideMenuAdapter.ViewHolder> {

    private final HomeActivity homeActivity;

    private ArrayList<SlideMenuItem> menuItems = new ArrayList<>();

    private LayoutInflater layoutInflater;

    private SlideMenuSelectionListener slideMenuSelectionListener;

    private int selectedPosition;

    public SlideMenuAdapter(HomeActivity homeActivity) {

        this.homeActivity = homeActivity;

        layoutInflater = LayoutInflater.from(homeActivity);
    }

    public ArrayList<SlideMenuItem> getMenuItems() {

        return menuItems;
    }

    public void setMenuItems(ArrayList<SlideMenuItem> menuItems) {

        if (menuItems == null) {

            menuItems = new ArrayList<>();
        }

        this.menuItems = menuItems;

        notifyDataSetChanged();
    }

    public void populate() {

        ArrayList<SlideMenuItem> slideMenuItems = new ArrayList<>();

        String name = homeActivity.getString(R.string.transaction_hitory);
        int iconDrawable = 0;
        int iconDrawableSelected = 0;

        SlideMenuItem item = new SlideMenuItem(name, iconDrawable, iconDrawableSelected, 0);
        slideMenuItems.add(item);

        name = homeActivity.getString(R.string.my_profile);

        SlideMenuItem itemAboutUs = new SlideMenuItem(name, iconDrawable, iconDrawableSelected, 1);
        slideMenuItems.add(itemAboutUs);

        name = homeActivity.getString(R.string.signout);

        SlideMenuItem itemSignout = new SlideMenuItem(name, iconDrawable, iconDrawableSelected, 2);
        slideMenuItems.add(itemSignout);

        setMenuItems(slideMenuItems);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = layoutInflater.inflate(R.layout.row_slide_menu, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final SlideMenuItem menuItem = menuItems.get(position);

        String name = menuItem.getName();
        holder.tvName.setText(name);

        final int menuItemPosition = menuItem.getId();
        if (menuItemPosition == selectedPosition) {

            int resId = menuItem.getIconDrawableSelected();

            if (resId > 0) {

                Drawable drawable = ContextCompat.getDrawable(homeActivity, resId);
                holder.ivIcon.setImageDrawable(drawable);

            } else {

                holder.ivIcon.setImageDrawable(null);
            }

            int selectedColor = homeActivity.getResources().getColor(R.color.colorPrimary);

            //holder.itemView.setBackgroundColor(selectedColor);

        } else {

            int resId = menuItem.getIconDrawable();

            if (resId > 0) {

                Drawable drawable = ContextCompat.getDrawable(homeActivity, resId);
                holder.ivIcon.setImageDrawable(drawable);

            } else {

                holder.ivIcon.setImageDrawable(null);
            }

            //holder.itemView.setBackgroundColor(Color.TRANSPARENT);
        }

        String badge = menuItem.getBadge();
        holder.tvBadge.setText(badge);

        if (!StringUtils.isEmpty(badge)) {

            holder.tvBadge.setVisibility(View.VISIBLE);

        } else {

            holder.tvBadge.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                /*if (selectedPosition == menuItemPosition) {
                    homeActivity.closeMenu();
                    return;
                }*/

                selectedPosition = menuItemPosition;

                notifyDataSetChanged();

                if (slideMenuSelectionListener != null) {

                    slideMenuSelectionListener.onSlideMenuItemSelected(menuItem);
                }

                homeActivity.closeMenu();
            }
        });

        //boolean playSound = AppPreferences.getBoolean(AppPreferences.PLAY_SOUND, false, homeActivity);
        //holder.switchSound.setChecked(playSound);

        /*holder.switchSound.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                boolean isChecked = holder.switchSound.isChecked();

                AppPreferences.setBoolean(AppPreferences.PLAY_SOUND, isChecked, homeActivity);

                if (isChecked) {

                    MusicPlayerService.playSound(homeActivity);

                } else {

                    MusicPlayerService.stopSound(homeActivity);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {

        return menuItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View itemView;

        public ImageView ivIcon;

        public TextView tvBadge;

        public TextView tvName;

        public Switch switchSound;

        public ViewHolder(View itemView) {

            super(itemView);

            this.itemView = itemView;

            tvName = (TextView) itemView.findViewById(R.id.tvName);

            tvBadge = (TextView) itemView.findViewById(R.id.tvBadge);

            ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);

            switchSound = (Switch) itemView.findViewById(R.id.tbSound);
        }
    }

    public SlideMenuSelectionListener getSlideMenuSelectionListener() {

        return slideMenuSelectionListener;
    }

    public void setSlideMenuSelectionListener(SlideMenuSelectionListener slideMenuSelectionListener) {

        this.slideMenuSelectionListener = slideMenuSelectionListener;
    }

    public interface SlideMenuSelectionListener {

        void onSlideMenuItemSelected(SlideMenuItem menuItem);
    }

    public int getSelectedPosition() {

        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {

        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }
}
