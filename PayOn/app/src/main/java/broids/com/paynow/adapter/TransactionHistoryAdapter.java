package broids.com.paynow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import broids.com.paynow.R;
import broids.com.paynow.activity.BaseActivity;
import broids.com.paynow.entity.TransactionBean;
import broids.com.paynow.utils.StringUtils;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder> {

    private ArrayList<TransactionBean> transactionBeans = new ArrayList<>();

    private BaseActivity activity;

    private LayoutInflater inflater;

    private OnItemClickListener onItemClickListener;

    public TransactionHistoryAdapter(BaseActivity activity) {

        this.activity = activity;

        inflater = LayoutInflater.from(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.row_transaction, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final TransactionBean bean = transactionBeans.get(position);

        String type = bean.getTrxType();
        String payeeName = bean.getPayeeName();
        String payerName = bean.getPayerName();
        String payeeVpa = bean.getPayeeVpa();
        String payerVpa = bean.getPayerVpa();
        String amount = bean.getAmount();

        String formattedAmount = formatAmount(amount);
        holder.tvAmount.setText(formattedAmount);

        String status = bean.getStatus();

        if(status != null) {

           status = status.substring(0, 1).toUpperCase() + status.substring(1).toLowerCase();
        }

        holder.tvTime.setText(status);

        if(TransactionBean.TRX_TYPE_PUSH.equalsIgnoreCase(type)){

            if(StringUtils.isEmpty(payeeName)){

                holder.tvName.setText(payeeVpa);

            } else {

                holder.tvName.setText(payeeName);
            }

            holder.tvHandle.setText(payeeVpa);

            holder.ivType.setImageResource(R.drawable.push);

        } else {

            if(StringUtils.isEmpty(payerName)){

                holder.tvName.setText(payerVpa);

            } else {

                holder.tvName.setText(payerName);
            }

            holder.tvHandle.setText(payerVpa);

            holder.ivType.setImageResource(R.drawable.pull);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (onItemClickListener != null) {

                    onItemClickListener.onItemSelected(bean, position);
                }

                notifyDataSetChanged();
            }
        });

    }

    private String formatAmount(String strAmount) {

        double amount = Double.parseDouble(strAmount);
        /*DecimalFormat formatter = new DecimalFormat("#,###.00");

        String result = formatter.format(amount);*/

        String result = "Rs. " + NumberFormat.getNumberInstance(Locale.US).format(amount);
        return result;
    }

    @Override
    public int getItemCount() {

        return transactionBeans.size();
    }

    public OnItemClickListener getOnItemClickListener() {

        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {

        this.onItemClickListener = onItemClickListener;
    }

    public ArrayList<TransactionBean> getTransactionBeans() {

        return transactionBeans;
    }

    public void setTransactionBeans(ArrayList<TransactionBean> transactionBeans) {

        if (transactionBeans == null) {

            transactionBeans = new ArrayList<>();
        }

        this.transactionBeans = transactionBeans;

        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public View itemView;

        public TextView tvName;

        public TextView tvHandle;

        public TextView tvAmount;

        public TextView tvTime;

        public ImageView ivType;

        public ViewHolder(View itemView) {

            super(itemView);

            this.itemView = itemView;

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvHandle = (TextView) itemView.findViewById(R.id.tvHandle);

            tvAmount = (TextView) itemView.findViewById(R.id.tvAmount);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);

            ivType = (ImageView) itemView.findViewById(R.id.ivType);
        }
    }

    public interface OnItemClickListener {

        void onItemSelected(TransactionBean transactionBean, int position);
    }
}
