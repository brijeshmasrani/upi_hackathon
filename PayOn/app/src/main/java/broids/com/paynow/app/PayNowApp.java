package broids.com.paynow.app;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by brijesh on 5/3/16.
 */
public class PayNowApp extends Application {

    @Override
    public void onCreate() {

        super.onCreate();

        ActiveAndroid.initialize(this);
    }
}
