package broids.com.paynow.dialog;

import broids.com.paynow.R;
import broids.com.paynow.activity.BaseActivity;

/**
 * Created by brijesh on 6/3/16.
 */
public class BeaconDialog extends ScanningEffectDialogBase {

    public BeaconDialog(BaseActivity context, DialogListener dialogListener) {

        super(context, dialogListener);
    }

    @Override
    protected String getTitle() {

        return activity.getString(R.string.beacon);
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }
}
