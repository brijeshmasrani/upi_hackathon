package broids.com.paynow.dialog;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;

import broids.com.paynow.R;
import broids.com.paynow.activity.BaseActivity;

/**
 * Created by brijesh on 6/3/16.
 */
public class BumpDialog extends ScanningEffectDialogBase {

    public BumpDialog(BaseActivity context, DialogListener dialogListener) {

        super(context, dialogListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    protected String getTitle() {

        return activity.getString(R.string.bump);
    }

}
