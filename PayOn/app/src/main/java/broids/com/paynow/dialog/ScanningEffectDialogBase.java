package broids.com.paynow.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;


import com.andexert.library.RippleView;

import java.util.Objects;

import broids.com.paynow.R;
import broids.com.paynow.activity.BaseActivity;

public abstract class ScanningEffectDialogBase extends Dialog {

    protected BaseActivity activity;

    protected TextView tvDialogTitle;

    protected Button btnClose;

    protected DialogListener dialogListener;

    public ScanningEffectDialogBase(BaseActivity context, DialogListener dialogListener) {

        super(context);

        this.activity = context;

        this.dialogListener = dialogListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_ripple_effect);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = this.getWindow();
        lp.copyFrom(window.getAttributes());

        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        initViews();
    }

    private void initViews() {

        Display display = ((WindowManager)activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int height = display.getHeight();
        int width = display.getWidth();

        final RippleView rl = (RippleView) findViewById(R.id.rl);
        rl.getLayoutParams().height = (int) (height*.58f);
        rl.getLayoutParams().width = width;
        rl.requestLayout();

        final float h = rl.getLayoutParams().height;
        final float w = rl.getLayoutParams().width;

        rl.animateRipple(w / 2, h / 2);

        rl.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

            @Override
            public void onComplete(RippleView rippleView) {

                rl.animateRipple(w / 2, h / 2);
            }
        });

        tvDialogTitle = (TextView) findViewById(R.id.tvDialogTitle);
        tvDialogTitle.setText(getTitle());

        btnClose = (Button) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                dismiss();
            }
        });
    }

    protected abstract String getTitle();


    @Override
    public void show() {

        getWindow().setWindowAnimations(R.style.DialogAnimation);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        super.show();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        dismiss();
    }

    public DialogListener getDialogListener() {

        return dialogListener;
    }

    public void setDialogListener(DialogListener dialogListener) {

        this.dialogListener = dialogListener;
    }

    public static interface DialogListener {

        void onSelected(Object o);
    }

}
