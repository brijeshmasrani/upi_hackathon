package broids.com.paynow.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brijesh on 5/3/16.
 */
@Table(name = "BankAccount")
public class BankAccountBean extends Model {

    @Column(name = "accountNumber", uniqueGroups = "bankAccount", onUniqueConflicts = Column.ConflictAction.REPLACE)
    private String accountNumber;

    @Column(name = "ifscCode")
    private String ifscCode;

    @Column(name = "paymentAddress", uniqueGroups = "bankAccount", onUniqueConflicts = Column.ConflictAction.REPLACE)
    private String paymentAddress;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getPaymentAddress() {

        return paymentAddress;
    }

    public void setPaymentAddress(String paymentAddress) {

        this.paymentAddress = paymentAddress;
    }

    public static ArrayList<BankAccountBean> getAllForPaymentAddress(String paymentAddress) {

        ArrayList<BankAccountBean> beans = new ArrayList<>();

        List<Model> models = new Select().from(BankAccountBean.class)
                .where("paymentAddress=?", paymentAddress)
                .orderBy(Table.DEFAULT_ID_NAME + " DESC")
                .execute();

        for(Model model : models) {

            BankAccountBean accountBean = (BankAccountBean) model;
            beans.add(accountBean);
        }

        return beans;
    }

    public static void deleteLinkedAccounts(String paymentAddress) {

        new Delete().from(BankAccountBean.class).where("paymentAddress=?", paymentAddress);
    }
}
