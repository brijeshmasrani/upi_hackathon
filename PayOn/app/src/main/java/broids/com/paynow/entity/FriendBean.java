package broids.com.paynow.entity;

import android.net.Uri;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

import broids.com.paynow.utils.StringUtils;

/**
 * Created by brijesh on 2/3/16.
 */
@Table(name = "Friends")
public class FriendBean extends Model implements Serializable {

    @Column(name = "contactId", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private String contactId;

    @Column(name = "name")
    private String name = "";

    @Column(name = "paymentAddress")
    private String paymentAddress = "";

    @Column(name = "photoUri")
    private String photoUri;

    public int getColorCode() {

        return colorCode;
    }

    public void setColorCode(int colorCode) {

        this.colorCode = colorCode;
    }

    @Column(name = "colorCode")
    private int colorCode;

    public String getContactId() {

        return contactId;
    }

    public void setContactId(String contactId) {

        this.contactId = contactId;
    }

    public String getName() {

        if (StringUtils.isEmpty(name)) {

            name = "";
        }

        return name;
    }

    public void setName(String name) {

        if (StringUtils.isEmpty(name)) {

            name = "";
        }

        this.name = name;
    }

    public Uri getPhotoUri() {

        if(StringUtils.isEmpty(photoUri)){

            return null;
        }

        return Uri.parse(photoUri);
    }

    public void setPhotoUri(Uri photoUri) {

        if(photoUri == null){

            return;
        }

        this.photoUri = photoUri.toString();
    }

    public String getPaymentAddress() {
        return paymentAddress;
    }

    public void setPaymentAddress(String paymentAddress) {
        this.paymentAddress = paymentAddress;
    }
}
