package broids.com.paynow.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NIKHIL on 3/20/2016.
 */
public class MerchantBean {

    @SerializedName("merchant_id")
    private String merchantId;

    @SerializedName("merchant_name")
    private String merchantName;

    @SerializedName("merchant_default_handle")
    private String merchantDefaultHandle;

    @SerializedName("short_address")
    private String shortAddress;

    @SerializedName("long_address")
    private String longAddress;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("email")
    private String email;

    @SerializedName("aadhaar_number")
    private String aadhaarNumber;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantDefaultHandle() {
        return merchantDefaultHandle;
    }

    public void setMerchantDefaultHandle(String merchantDefaultHandle) {
        this.merchantDefaultHandle = merchantDefaultHandle;
    }

    public String getShortAddress() {
        return shortAddress;
    }

    public void setShortAddress(String shortAddress) {
        this.shortAddress = shortAddress;
    }

    public String getLongAddress() {
        return longAddress;
    }

    public void setLongAddress(String longAddress) {
        this.longAddress = longAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAadhaarNumber() {
        return aadhaarNumber;
    }

    public void setAadhaarNumber(String aadhaarNumber) {
        this.aadhaarNumber = aadhaarNumber;
    }
}
