package broids.com.paynow.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by brijesh on 5/3/16.
 */
@Table(name = "PaymentAddress")
public class PaymentAddressBean extends Model {

    @Column(name = "paymentAddress", unique = true, onUniqueConflicts = Column.ConflictAction.REPLACE)
    private String paymentAddress;

    private ArrayList<BankAccountBean> bankAccounts = new ArrayList<>();

    public ArrayList<BankAccountBean> getBankAccounts() {

        if(bankAccounts == null) {

            bankAccounts = new ArrayList<>();
        }

        return bankAccounts;
    }

    public void setBankAccounts(ArrayList<BankAccountBean> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public String getPaymentAddress() {

        if(paymentAddress == null) {

            paymentAddress = "";
        }

        return paymentAddress;
    }

    public void setPaymentAddress(String paymentAddress) {

        this.paymentAddress = paymentAddress;
    }

    public static ArrayList<PaymentAddressBean> getAll() {

        ArrayList<PaymentAddressBean> beans = new ArrayList<>();

        List<Model> models = new Select().from(PaymentAddressBean.class)
                .orderBy(Table.DEFAULT_ID_NAME + " DESC")
                .execute();

        for (Model model : models) {

            PaymentAddressBean address = (PaymentAddressBean) model;
            beans.add(address);

            String paymentAddress = address.getPaymentAddress();

            ArrayList<BankAccountBean> accounts = BankAccountBean.getAllForPaymentAddress(paymentAddress);
            address.setBankAccounts(accounts);
        }

        return beans;
    }

    public static ArrayList<String> getAllAddresses() {

        ArrayList<String> addresses = new ArrayList<>();

        List<Model> models = new Select().from(PaymentAddressBean.class)
                .orderBy(Table.DEFAULT_ID_NAME + " DESC")
                .execute();

        for (Model model : models) {

            PaymentAddressBean address = (PaymentAddressBean) model;
            String paymentAddress = address.getPaymentAddress();

            addresses.add(paymentAddress);
        }

        return addresses;
    }

    public static PaymentAddressBean getPaymentAddress(String paymentAddress) {

        PaymentAddressBean paymentAddressBean = new Select().from(PaymentAddressBean.class)
                .where("paymentAddress=?", paymentAddress)
                .orderBy(Table.DEFAULT_ID_NAME + " DESC")
                .executeSingle();

        return paymentAddressBean;
    }

}
