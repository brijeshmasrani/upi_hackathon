package broids.com.paynow.entity;

/**
 * Created by Softweb Solutions on 19-Dec-15.
 */
public class SlideMenuItem {

    private String name;

    private int iconDrawable;

    private int iconDrawableSelected;

    private String badge;

    private int id;

    public SlideMenuItem(String name, int iconDrawable, int iconDrawableSelected, int id) {

        this.name = name;
        this.iconDrawable = iconDrawable;
        this.id = id;
        this.iconDrawableSelected = iconDrawableSelected;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getIconDrawable() {

        return iconDrawable;
    }

    public void setIconDrawable(int iconDrawable) {

        this.iconDrawable = iconDrawable;
    }

    public int getIconDrawableSelected() {

        return iconDrawableSelected;
    }

    public void setIconDrawableSelected(int iconDrawableSelected) {

        this.iconDrawableSelected = iconDrawableSelected;
    }

    public String getBadge() {

        return badge;
    }

    public void setBadge(String badge) {

        this.badge = badge;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }
}
