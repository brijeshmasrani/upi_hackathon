package broids.com.paynow.entity;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

/**
 * Created by NIKHIL on 3/20/2016.
 */
@Table(name = "TransactionBeans")
public class TransactionBean extends Model{

    public static final String TRX_TYPE_PUSH = "PUSH";
    public static final String TRX_TYPE_PULL = "PULL";

    public static final String TRX_STATUS_SUCCESS = "SUCCESS";
    //public static final String TRX_STATUS_ERROR = "ERROR";
    public static final String TRX_STATUS_PENDING = "PENDING";
    public static final String TRX_STATUS_FAILED = "FAILED";
    //public static final String TRX_STATUS_INITIATED = "INITIATED";

    @Column(name = "upi_transaction_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    @SerializedName("upi_transaction_id")
    private String upiTransactionId;

    @Column(name = "payer_id")
    @SerializedName("payer_id")
    private String payerId;

    @Column(name = "payer_vpa")
    @SerializedName("payer_vpa")
    private String payerVpa;

    @Column(name = "created_on")
    @SerializedName("created_on")
    private Long createdOnTimeStamp;

    @Column(name = "payee_vpa")
    @SerializedName("payee_vpa")
    private String payeeVpa;

    @Column(name = "amount")
    @SerializedName("amount")
    private String amount;

    @Column(name = "trx_type")
    @SerializedName("trx_type")
    private String trxType;

    @Column(name = "status")
    @SerializedName("status")
    private String status;

    @Column(name = "payee_id")
    @SerializedName("payee_id")
    private String payeeId;

    @Column(name = "description")
    @SerializedName("description")
    private String description;

    @Column(name = "payee_name")
    @SerializedName("payee_name")
    private String payeeName;

    @Column(name = "payer_name")
    @SerializedName("payer_name")
    private String payerName;

    public String getUpiTransactionId() {
        return upiTransactionId;
    }

    public void setUpiTransactionId(String upiTransactionId) {
        this.upiTransactionId = upiTransactionId;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }

    public String getPayerVpa() {
        return payerVpa;
    }

    public void setPayerVpa(String payerVpa) {
        this.payerVpa = payerVpa;
    }

    public String getPayeeVpa() {
        return payeeVpa;
    }

    public void setPayeeVpa(String payeeVpa) {
        this.payeeVpa = payeeVpa;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public long getCreatedOnTimeStamp() {
        return createdOnTimeStamp;
    }

    public void setCreatedOnTimeStamp(long createdOnTimeStamp) {
        this.createdOnTimeStamp = createdOnTimeStamp;
    }
}
