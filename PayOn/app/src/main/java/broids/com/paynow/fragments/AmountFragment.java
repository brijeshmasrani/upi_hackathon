package broids.com.paynow.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import broids.com.paynow.R;
import broids.com.paynow.entity.PaymentAddressBean;
import broids.com.paynow.entity.TransactionBean;
import broids.com.paynow.utils.Notify;
import broids.com.paynow.utils.StringUtils;
import broids.com.paynow.ws.entity.AadhaarKycBean;

/**
 * Created by brijesh on 28/2/16.
 */
public class AmountFragment extends BaseFragment {

    public static final String FRIEND_NAME = "friend_bean";
    public static final String PAYMENT_ADDRESS = "payment_address";
    private static final String IS_ASKING = "is-asking";

    private TextView tvPaymentAddress;

    private Button btnSendMoney;

    private Context mContext;

    private EditText etAmount;

    private ImageView ivStatusBar;

    private AutoCompleteTextView etPaymentAddress;

    private boolean isAsking;

    private String payeeName;
    private String payerAcAddressType;
    private String payerva;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    protected int getContentView() {

        return R.layout.fragment_amount;
    }

    @Override
    protected void initViews(View rootView) {

        tvPaymentAddress = (TextView) rootView.findViewById(R.id.tvPaymentAddress);
        tvPaymentAddress.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                finish();
            }
        });

        btnSendMoney = (Button) rootView.findViewById(R.id.btnSendMoney);
        btnSendMoney.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Notify.showSnackBar(v, "To be implemented ...", (Activity) v.getContext());
                sendPaymentRequestToUPI();
            }
        });

        disableSend();

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0
                        && etPaymentAddress.getText().length() > 0) {

                    enableSend();

                } else {

                    disableSend();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        etAmount = (EditText) rootView.findViewById(R.id.etAmount);
        etAmount.addTextChangedListener(watcher);

        TextWatcher watcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0
                        && etAmount.getText().length() > 0) {

                    enableSend();

                } else {

                    disableSend();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        ivStatusBar = (ImageView) rootView.findViewById(R.id.ivStatusBar);

        etPaymentAddress = (AutoCompleteTextView) rootView.findViewById(R.id.etPaymentAddress);
        etPaymentAddress.addTextChangedListener(watcher1);

        ArrayList<String> addresses = PaymentAddressBean.getAllAddresses();

        String[] toArray = new String[addresses.size()];

        for (int i = 0; i < addresses.size(); i++) {

            toArray[i] = addresses.get(i);
        }

        if(toArray.length > 0) {

            etPaymentAddress.setText(toArray[0]);
        }

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, toArray);

        etPaymentAddress.setAdapter(adapter);

        Bundle bundle = getArguments();

        isAsking = bundle.getBoolean(IS_ASKING);

        if(isAsking){

            btnSendMoney.setText(R.string.ask_for_money);
        }

        String payeeAddress = bundle.getString(PAYMENT_ADDRESS);

        String mapp = getString(R.string.mapp);

        if(!payeeAddress.contains(mapp)) {

            payeeAddress = payeeAddress + mapp;
        }

//        tvPaymentAddress.setText(R.string.text_set_amount);
    }

    private void enableSend() {

        btnSendMoney.setEnabled(true);
        if(!isAsking) {

            btnSendMoney.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));

        } else {

            btnSendMoney.setBackgroundColor(activity.getResources().getColor(R.color.green));
        }
    }

    private void disableSend() {

        btnSendMoney.setEnabled(false);
        btnSendMoney.setBackgroundColor(activity.getResources().getColor(R.color.grey_text));
    }

    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    protected String getTitle() {

        String title = "Pay";
        return title;
    }

    @Override
    protected void loadData() {

    }

    public static AmountFragment getInstance(String friendName, String paymentAddress, boolean isAsking) {

        AmountFragment fragment = new AmountFragment();

        Bundle bundle = new Bundle();
        bundle.putString(FRIEND_NAME, friendName);
        bundle.putString(PAYMENT_ADDRESS, paymentAddress);
        bundle.putBoolean(IS_ASKING, isAsking);

        fragment.setArguments(bundle);

        return fragment;
    }

    void sendPaymentRequestToUPI() {

        StringBuilder urlBuilder = new StringBuilder();
        int order = 100001 % new Random().nextInt();

        Bundle bundle = getArguments();

        String amount = etAmount.getText().toString();
        payeeName = bundle.getString(FRIEND_NAME, "brijesh");
        String PVA = bundle.getString(PAYMENT_ADDRESS); // Keep this MAPP only

        payerva = etPaymentAddress.getText().toString();

        String mapp = getString(R.string.mapp);

        if(payerva.contains(mapp)) {

            payerva = payerva.replace("@MAPP","");
        }

        int orderId = 100001 % new Random().nextInt();
        urlBuilder.append("upi://pay").append("?").
                append("amount").append("=").
                append(amount).
                append("&").append("ORDERID").append("=")
                .append(Integer.parseInt("675")
                        + orderId).append("&").append("credit_flag").append("=").append("1").
                append("&").append("appname").append("=").append("MGSAPP").append("&").
                append("am").append("=").append(amount).
                append("&").append("tn").
                append("=").append(Integer.parseInt("675") + orderId).
                append("&").append("tr").append("=").append("Test for UPI").
                append("&").append("ti").append("=").
                append(Integer.parseInt("675") + orderId).
                append("&").append("appid").append("=").append("1116").append("&").append("cu").
                append("=").append("INR").append("&").append("gcode").
                append("=").append("123466454").
                append("&").append("location").append("=").append("Mumbai,Maharashtra").append("&").
                append("ip").append("=").append("154fer53urn").append("&").append("os").append("=").
                append("android").append("&").append("payerAcAddressType").append("=").
                append("Savings").append("&").
                append("capability").append("=").append("453453d4f5343434df354").append("&").
                append("payeeName").append("=").append(payeeName).
                append("&").append("payeeType").append("=").append("PERSON").append("&").
                append("PVA").append("=").append(PVA).
                append("&").append("PayerVA").append("=").
                append(payerva);


        String deepLinkUrl = urlBuilder.toString();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(deepLinkUrl));
        Intent chooser = Intent.createChooser(intent, "Pay With");
        if (intent.resolveActivity(mContext.getPackageManager()) != null) {
            getActivity().startActivityForResult(chooser, 1);
        }

        // Move this code
       /* String name = payeeName;
        String pva = PVA;
        String amountX =  amount;
        String transId = "transactionId";

        DoneFragment doneFragment = DoneFragment.getInstance(name, pva, amountX, transId, isAsking);
        activity.pushFragments(doneFragment, null, true, true, false);

        String userName = AadhaarKycBean.getKyc(activity).getName();

        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setAmount(amountX);
        transactionBean.setDescription("From android");
        transactionBean.setPayeeId(pva);
        transactionBean.setPayerId(payerva);
        transactionBean.setPayeeVpa(pva);
        transactionBean.setPayerVpa(payerva);
        transactionBean.setPayerName(userName);
        transactionBean.setPayeeName(name);
        transactionBean.setStatus(TransactionBean.TRX_STATUS_PENDING);

        if(isAsking) {
            transactionBean.setTrxType(TransactionBean.TRX_TYPE_PULL);
        } else {
            transactionBean.setTrxType(TransactionBean.TRX_TYPE_PUSH);
        }
        transactionBean.setUpiTransactionId(transId);
        transactionBean.save();*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {

            Bundle bundle = data.getExtras();

            if (bundle == null)
                return;

            String PayeeVirtualAddress = bundle.getString("Payee Virtual Address");
            String transactionId = bundle.getString("Transaction Id");
            String transactionRemark = bundle.getString("Transaction Remarks"); // Either Success or Failure

            // Move this code
            String name = payeeName;
            String pva = PayeeVirtualAddress;
            String amountX =  bundle.getString("Amount");;
            String transId = transactionId;
            DoneFragment doneFragment = DoneFragment.getInstance(name, pva, amountX, transId, isAsking);
            activity.pushFragments(doneFragment, null, true, true, false);

            String userName = AadhaarKycBean.getKyc(activity).getName();

            TransactionBean transactionBean = new TransactionBean();
            transactionBean.setAmount(amountX);
            transactionBean.setDescription("From android");
            transactionBean.setPayeeId(pva);
            transactionBean.setPayerId(payerva);
            transactionBean.setPayeeVpa(pva);
            transactionBean.setPayerVpa(payerva);
            transactionBean.setPayerName(userName);
            transactionBean.setPayeeName(name);
            transactionBean.setStatus(TransactionBean.TRX_STATUS_PENDING);

            if(isAsking) {
                transactionBean.setTrxType(TransactionBean.TRX_TYPE_PULL);
            } else {
                transactionBean.setTrxType(TransactionBean.TRX_TYPE_PUSH);
            }
            transactionBean.setUpiTransactionId(transId);
            transactionBean.save();
        }

       /* Bundle b = new Bundle();
        b.putString("Amount", Transaction_Success.this.amt);
        b.putString("Currency", "INR");
        b.putString("Payee Virtual Address", this.val$Payeraddress);
        b.putString("Transaction Id", this.val$Tranid);
        b.putString("Payee Virtual Address", "sachin@mgat");
        b.putString("Transaction Ref No", this.val$Tranid);
        b.putString("Transaction Remarks", "success");
        b.putString("App Id", Transaction_Success.this.getApplicationContext().getPackageName());
        b.putString("App Name", "MGAT_PSP");
        Intent i = Transaction_Success.this.getIntent();
        i.putExtra(NotificationCompatApi21.CATEGORY_STATUS, "success");
        i.putExtra("res_data", b);
        Transaction_Success.this.setResult(-1, i);
        Transaction_Success.this.finish();*/
    }


}
