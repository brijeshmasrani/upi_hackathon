package broids.com.paynow.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import broids.com.paynow.activity.HomeActivity;


/**
 * Created by Softweb Solutions on 15-Jul-15.
 */
public abstract class BaseFragment extends Fragment {

    public static String REQUEST_CODE = "request_code";

    protected View rootView;

    protected HomeActivity activity;

    protected TextView tvLanguage;

    //protected TextView tvNoData;

    //protected SwipeRefreshLayout swipeRefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (rootView != null) {

            return rootView;
        }

        rootView = inflater.inflate(getContentView(), container, false);

        initViews(rootView);

        initCommonViews();

        activity.setTitle(getTitle());

        //setTextLabels();

        setAppTheme();

        return rootView;
    }

    protected void initCommonViews() {

        //swipeRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefresh);

        /*if (swipeRefresh != null) {

            swipeRefresh.setColorSchemeResources(R.color.colorAccent);

            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                @Override
                public void onRefresh() {

                    BaseFragment.this.onRefresh();
                }
            });
        }*/

    }

    protected void hideSwipeRefreshLayout(boolean isHide, String strNoData) {

        //Notify.dialogOK(strNoData, activity, false);

        //tvNoData = (TextView) rootView.findViewById(R.id.tvNoData);

        /*if(tvNoData != null) {

            if(isHide) {

                //swipeRefresh.setVisibility(View.GONE);
                tvNoData.setVisibility(View.VISIBLE);
                tvNoData.setText(strNoData);
            } else {

                swipeRefresh.setVisibility(View.VISIBLE);
                tvNoData.setVisibility(View.GONE);
            }
        }*/
    }


    protected void onRefresh() {

        loadData();
    }


    @Override
    public void onHiddenChanged(boolean hidden) {

        activity.setTitle(getTitle());
    }

    @Override
    public void onResume() {

        super.onResume();

        activity.setTitle(getTitle());

        setTextLabels();
    }


    public void setTitle(CharSequence title) {

        activity.setTitle(title);
    }

    protected abstract int getContentView();

    protected abstract void initViews(View rootView);

    protected abstract void setTextLabels();

    protected void setAppTheme() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    protected abstract String getTitle();

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);

        this.activity = (HomeActivity) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        loadData();
    }

    protected abstract void loadData();

    //finish the fragment!
    public boolean finish() {

        /*if(activity instanceof HomeActivity) {

            HomeActivity homeActivity = (HomeActivity) activity;

            homeActivity.popFragment();
        }*/

        activity.popFragment();

        return true;
    }

    public void finishWithResult(Bundle result) {

        Bundle bundle = getArguments();

        if (bundle != null
                && bundle.containsKey(REQUEST_CODE)) {

            int requestCode = bundle.getInt(REQUEST_CODE);

            BaseFragment fragment = activity.getCallingFragment(requestCode);
            if (fragment != null) {

                fragment.onFragmentResult(result);
            }

            activity.removeCallingFragment(requestCode);
        }

        finish();
    }

    public void showProgressDialog() {

        if (isAdded()) {

            /*if (swipeRefresh != null) {

                swipeRefresh.post(new Runnable() {

                    @Override
                    public void run() {

                        swipeRefresh.setRefreshing(true);
                    }
                });


            } else {

                activity.showProgressDialog();
            }*/

            activity.showProgressDialog();

        }
    }

    public void dismissProgressDialog() {

        if (isAdded()) {

            /*if (swipeRefresh != null) {

                swipeRefresh.setRefreshing(false);

            }*/

            activity.dismissProgressDialog();
        }
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {

        //super.onCreateOptionsMenu(menu, menuInflater);

        menu.clear();
        menuInflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_settings:
                Notify.toast("Settings!", activity);
                return true;

        }
        return false;
    }*/

    protected void startFragmentForResult(BaseFragment baseFragment, int requestCode) {

        activity.addCallingFragment(this, requestCode);

        baseFragment.getArguments().putInt(REQUEST_CODE, requestCode);

        activity.pushFragments(baseFragment, null, true, true, false);
    }

    protected void onFragmentResult(Bundle bundle) {

    }

}
