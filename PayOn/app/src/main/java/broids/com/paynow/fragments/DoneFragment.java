package broids.com.paynow.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import broids.com.paynow.R;
import broids.com.paynow.entity.PaymentAddressBean;

/**
 * Created by brijesh on 28/2/16.
 */
public class DoneFragment extends BaseFragment {

    public static final String FRIEND_NAME = "friend_bean";
    public static final String AMOUNT = "amount";
    public static final String TRANSACTION_ID = "transactionID";
    public static final String PAYMENT_ADDRESS = "payment_address";
    private static final String IS_ASKING = "is-asking";

    private TextView tvHandle;
    private TextView tvName;
    private TextView tvEnterAmount;

    private Button btnDone;

    private Context mContext;

    private boolean isAsking;


    public static DoneFragment getInstance(String friendName, String paymentAddress, String amount, String transId, boolean isAsking) {

        DoneFragment fragment = new DoneFragment();

        Bundle bundle = new Bundle();
        bundle.putString(FRIEND_NAME, friendName);
        bundle.putString(PAYMENT_ADDRESS, paymentAddress);
        bundle.putBoolean(IS_ASKING, isAsking);
        bundle.putString(AMOUNT, amount);
        bundle.putString(TRANSACTION_ID, transId);

        fragment.setArguments(bundle);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    protected int getContentView() {

        return R.layout.fragment_done;
    }

    @Override
    protected void initViews(View rootView) {


        btnDone = (Button) rootView.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                finish();
            }
        });

        tvEnterAmount = (TextView) rootView.findViewById(R.id.tvEnterAmount);
        tvName = (TextView) rootView.findViewById(R.id.tvName);
        tvHandle = (TextView) rootView.findViewById(R.id.tvHandle);

        Bundle bundle = getArguments();

        isAsking = bundle.getBoolean(IS_ASKING);

        if(isAsking){

        }

        String amount = bundle.getString(AMOUNT);
        String orderId = bundle.getString(TRANSACTION_ID);
        String payeeName = bundle.getString(FRIEND_NAME, "brijesh");
        String PVA = bundle.getString(PAYMENT_ADDRESS); // Keep this MAPP only

        TextView tvId = (TextView) rootView.findViewById(R.id.tvId);
        tvId.setText(orderId);

        tvEnterAmount.setText("Rs. "+amount);
        tvName.setText(payeeName);
        tvHandle.setText(PVA);
    }

    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    protected String getTitle() {

        String title = "Pay";
        return title;
    }

    @Override
    protected void loadData() {

    }
}
