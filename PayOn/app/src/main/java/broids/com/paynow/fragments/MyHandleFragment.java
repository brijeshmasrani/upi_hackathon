package broids.com.paynow.fragments;

import android.view.View;

import broids.com.paynow.R;

/**
 * Created by brijesh on 28/2/16.
 */
public class MyHandleFragment extends BaseFragment {

    @Override
    protected int getContentView() {

        return R.layout.activity_profile;
    }

    @Override
    protected void initViews(View rootView) {

    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    protected String getTitle() {

        return "My Payment Address";
    }

    @Override
    protected void loadData() {

    }

    public static MyHandleFragment getInstance() {

        MyHandleFragment fragment = new MyHandleFragment();
        return fragment;
    }
}
