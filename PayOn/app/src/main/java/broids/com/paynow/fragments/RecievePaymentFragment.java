package broids.com.paynow.fragments;

import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.android.volley.NetworkResponse;
import com.mobstac.beaconstac.core.Beaconstac;
import com.mobstac.beaconstac.core.BeaconstacReceiver;
import com.mobstac.beaconstac.core.MSConstants;
import com.mobstac.beaconstac.core.MSPlace;
import com.mobstac.beaconstac.core.Webhook;
import com.mobstac.beaconstac.models.MSAction;
import com.mobstac.beaconstac.models.MSBeacon;
import com.mobstac.beaconstac.utils.MSException;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.ViewHolder;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import broids.com.paynow.R;
import broids.com.paynow.activity.FriendsActivity;
import broids.com.paynow.dialog.BumpDialog;
import broids.com.paynow.dialog.ScanningEffectDialogBase;
import broids.com.paynow.entity.FriendBean;
import broids.com.paynow.utils.CameraPreview;
import broids.com.paynow.utils.Notify;
import broids.com.paynow.ws.AppWs;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;
import broids.com.paynow.ws.entity.GetMerchantByUUIDRequest;
import broids.com.paynow.ws.entity.GetMerchantByUUIDResponse;

/**
 * Created by brijesh on 28/2/16.
 */
public class RecievePaymentFragment extends BaseFragment {

    private FloatingActionButton fabReceieve;

    private Button btnSendRequest;
    private Button btnFriends;
    private Button btnBump;
    private Button btnBeacon;
    private Button btnQRCode;
    private EditText etPaymentAddress;
    private Beaconstac bstac;
    BumpDialog bumpDialog = null;

    Context mContext;
    NfcAdapter nfcAdapter;
    boolean isNFCAvailable;
    PendingIntent nfcPendingIntent;
    IntentFilter[] intentFiltersArray;

    static {
        System.loadLibrary("iconv");
    }

    DialogPlus mDialogPlus;
    ImageScanner scanner;
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    private boolean barcodeScanned = false;
    private boolean previewing = true;
    private BluetoothAdapter mBluetoothAdapter;
    private static final int REQUEST_ENABLE_BT = 1;
    private BeaconAdapter beaconAdapter;
    private ArrayList<MSBeacon> beacons = new ArrayList<>();
    private String friendName;

    @Override
    protected int getContentView() {
        return R.layout.fragment_recieve;
    }


    @Override
    protected void initViews(View rootView) {
        mContext = getActivity();

        NfcManager manager = (NfcManager) mContext.getSystemService(Context.NFC_SERVICE);
        nfcAdapter = manager.getDefaultAdapter();

        fabReceieve = (FloatingActionButton) rootView.findViewById(R.id.fabRecieve);
        fabReceieve.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                finish();
            }
        });

        btnSendRequest = (Button) rootView.findViewById(R.id.btnSendRequest);
        btnSendRequest.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                sendMoney();
            }
        });

        btnFriends = (Button) rootView.findViewById(R.id.btnFriends);
        btnFriends.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                friends();
            }
        });

        disableSend();
        etPaymentAddress = (EditText) rootView.findViewById(R.id.etPaymentAddress);
        etPaymentAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length() > 0) {

                    enableSend();

                } else {

                    disableSend();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnBeacon = (Button) rootView.findViewById(R.id.btnBeacon);
        btnBeacon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showBeacon();

            }
        });

        btnBump = (Button) rootView.findViewById(R.id.btnBump);
        btnBump.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onNFCclicked();
                if (nfcAdapter != null) {
                    nfcAdapter.enableForegroundDispatch(getActivity(),
                            nfcPendingIntent, intentFiltersArray, null);
                }

                bumpDialog = new BumpDialog(activity, new ScanningEffectDialogBase.DialogListener() {

                    @Override
                    public void onSelected(Object o) {

                    }
                });

                bumpDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        if (nfcAdapter != null)
                            nfcAdapter.disableForegroundDispatch(getActivity());
                    }
                });

                bumpDialog.show();
            }

            private void
            onNFCclicked() {

                isNFCAvailable = mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC);

                if (isNFCAvailable) {
                    Intent nfcIntent = new Intent(mContext, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    nfcPendingIntent = PendingIntent.getActivity(mContext, 0, nfcIntent, 0);

                    // Create an Intent Filter limited to the URI or MIME type to
                    // intercept TAG scans from.
                    IntentFilter tagIntentFilter =
                            new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
                    try {
                        tagIntentFilter.addDataType("application/paynow");
                        intentFiltersArray = new IntentFilter[]{tagIntentFilter};
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
        });

        btnQRCode = (Button) rootView.findViewById(R.id.btnQRCode);
        btnQRCode.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showQRDialog();
            }
        });
    }

    private void enableSend() {

        btnSendRequest.setEnabled(true);
        btnSendRequest.setBackgroundColor(activity.getResources().getColor(R.color.green));
    }

    private void disableSend() {

        btnSendRequest.setEnabled(false);
        btnSendRequest.setBackgroundColor(activity.getResources().getColor(R.color.grey_text));

    }

    public void processNFC(String handle) {
        if (etPaymentAddress != null)
            etPaymentAddress.setText(handle);

        if (bumpDialog != null)
            bumpDialog.dismiss();
    }

    private void showQRDialog() {

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();
        barcodeScanned = false;
        previewing = true;
                /* Instance barcode scanner */
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(getActivity(), mCamera, previewCb, autoFocusCB);

        mDialogPlus = DialogPlus.newDialog(getActivity())
                .setContentHolder(new ViewHolder(R.layout.activity_qrreader))
                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialog) {
                        releaseCamera();
                    }
                }).create();

        FrameLayout preview = (FrameLayout) mDialogPlus.findViewById(R.id.cameraPreview);
        Display display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int height = display.getHeight();
        int width = display.getWidth();
        preview.getLayoutParams().height = (int) (height * .45f);
        preview.getLayoutParams().width = width;
        preview.requestLayout();
        preview.addView(mPreview);

        Button close = (Button) mDialogPlus.findViewById(R.id.btnClose);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogPlus.dismiss();
            }
        });

        if (barcodeScanned) {
            barcodeScanned = false;
//            txtResult.setText("Scanning...");
            mCamera.setPreviewCallback(previewCb);
            mCamera.startPreview();
            previewing = true;
            mCamera.autoFocus(autoFocusCB);
        }

        mDialogPlus.show();
    }

    @Override
    protected void setTextLabels() {

    }

    private void friends() {

        Intent intent = new Intent(activity, FriendsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, FriendsActivity.REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            if (data != null
                    && data.hasExtra(FriendsActivity.SELECTED_FRIEND)) {

                FriendBean bean = (FriendBean) data.getSerializableExtra(FriendsActivity.SELECTED_FRIEND);

                String address = bean.getPaymentAddress();

                etPaymentAddress.setText(address);
                etPaymentAddress.setSelection(address.length());

                friendName = bean.getName();
                Notify.toast(friendName, activity);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected String getTitle() {

        return activity.getString(R.string.ask);
    }

    @Override
    protected void loadData() {

    }

    private void sendMoney() {

        String paymentAddress = etPaymentAddress.getText().toString();
        AmountFragment amountFragment = AmountFragment.getInstance(friendName, paymentAddress, true);

        activity.pushFragments(amountFragment, null, true, true, false);
    }

    public static RecievePaymentFragment getInstance() {

        RecievePaymentFragment fragment = new RecievePaymentFragment();
        return fragment;
    }


    void showBeacon() {
        // Use this check to determine whether BLE is supported on the device.
        if (!mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(mContext, "BLE not supported on this device.", Toast.LENGTH_SHORT).show();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            BluetoothManager mBluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = mBluetoothManager.getAdapter();
        }

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Log.e("Instapay Beacon", "Unable to obtain a BluetoothAdapter.");
            Toast.makeText(mContext, "Unable to obtain a BluetoothAdapter", Toast.LENGTH_LONG).show();

        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }

        bstac = Beaconstac.getInstance(mContext);
        bstac.setRegionParams("F94DBB23-2266-7822-3782-57BEAC0952AC", "com.mobstac.beaconstacdemo");
        bstac.syncRules();

        try {
            bstac.startRangingBeacons();
        } catch (MSException e) {
            e.printStackTrace();
        }

        init();
    }

    DialogPlus beaconSearchDialog = null;

    private void init() {
        beaconAdapter = new BeaconAdapter(beacons, mContext);

        beaconSearchDialog = DialogPlus.newDialog(mContext)
                .setAdapter(beaconAdapter)
                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialog) {
                        if (bstac != null)
                            try {
                                bstac.stopRangingBeacons();
                            } catch (MSException e) {
                                e.printStackTrace();
                            }
                    }
                })
                .create();

        registerBroadcast();
        beaconSearchDialog.show();
    }

    private boolean registered = false;
    private boolean isBeaconDetected = false;

    private void registerBroadcast() {
        if (!registered) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(MSConstants.BEACONSTAC_INTENT_RANGED_BEACON);
            intentFilter.addAction(MSConstants.BEACONSTAC_INTENT_CAMPED_BEACON);
            intentFilter.addAction(MSConstants.BEACONSTAC_INTENT_EXITED_BEACON);
            intentFilter.addAction(MSConstants.BEACONSTAC_INTENT_RULE_TRIGGERED);
            intentFilter.addAction(MSConstants.BEACONSTAC_INTENT_ENTERED_REGION);
            intentFilter.addAction(MSConstants.BEACONSTAC_INTENT_EXITED_REGION);
            mContext.registerReceiver(myBroadcastReceiver, intentFilter);
            registered = true;
            MSAction.handler = new Webhook() {
                @Override
                public void onWebHookResponse(NetworkResponse response) {
                    Toast.makeText(mContext, "Webhook completed", Toast.LENGTH_SHORT).show();
                }
            };
        }
    }

    BeaconstacReceiver myBroadcastReceiver = new BeaconstacReceiver() {
        @Override
        public void rangedBeacons(Context context, ArrayList<MSBeacon> arrayList) {
            if (!isBeaconDetected) {
                beaconAdapter.clear();
                beacons.addAll(arrayList);
                beaconAdapter.notifyDataSetChanged();
                System.out.println("brijesh rangedBeacons " + arrayList.get(0).getBeaconKey());

            }
            if (arrayList != null && arrayList.size() > 0)
                isBeaconDetected = true;

        }

        @Override
        public void campedOnBeacon(Context context, MSBeacon msBeacon) {

        }

        @Override
        public void exitedBeacon(Context context, MSBeacon msBeacon) {

        }

        @Override
        public void triggeredRule(Context context, String s, ArrayList<MSAction> arrayList) {

        }

        @Override
        public void enteredRegion(Context context, String s) {
        }

        @Override
        public void exitedRegion(Context context, String s) {

        }

        @Override
        public void enteredGeofence(Context context, ArrayList<MSPlace> arrayList) {

        }

        @Override
        public void exitedGeofence(Context context, ArrayList<MSPlace> arrayList) {

        }
    };


    //QR code

    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {
//                    txtResult.setText("barcode result " + sym.getData());
                    etPaymentAddress.setText(sym.getData());
                    etPaymentAddress.setSelection(etPaymentAddress.getText().length());

                    barcodeScanned = true;
                }
                mDialogPlus.dismiss();
            }
        }
    };
    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };
    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.cancelAutoFocus();
            mCamera.setPreviewCallback(null);
            mCamera.release();
        }
    }

    public class BeaconAdapter extends BaseAdapter {
        private ArrayList<MSBeacon> beacons;
        private Context ctx;
        private LayoutInflater myInflator;
        int height, width;

        public BeaconAdapter(ArrayList<MSBeacon> arr, Context c) {
            super();
            beacons = arr;
            ctx = c;
            myInflator = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Display display = ((WindowManager) activity.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            height = display.getHeight();
            width = display.getWidth();
        }

        public void addBeacon(MSBeacon beacon) {
            if (!beacons.contains(beacon)) {
                beacons.add(beacon);
            }
        }

        public void removeBeacon(MSBeacon beacon) {
            if (beacons.contains(beacon)) {
                beacons.remove(beacon);
            }
        }

        public void clear() {
            beacons.clear();
        }

        @Override
        public int getCount() {
            return beacons.size();
        }

        @Override
        public MSBeacon getItem(int position) {
            return beacons.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void notifyDataSetChanged() {
            Collections.sort(beacons, new Comparator<MSBeacon>() {
                @Override
                public int compare(MSBeacon lhs, MSBeacon rhs) {
                    if (lhs.getLatestRSSI() > rhs.getLatestRSSI())
                        return -1;
                    else if (lhs.getLatestRSSI() < rhs.getLatestRSSI())
                        return 1;
                    else
                        return 0;
                }
            });

            super.notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {

            if (view == null) {
                view = myInflator.inflate(R.layout.dialog_ripple_effect, parent, false);
            }

            final RippleView rl = (RippleView) view.findViewById(R.id.rl);
            rl.getLayoutParams().height = (int) (height * .45f);
            rl.getLayoutParams().width = width;
            rl.requestLayout();

            final float h = rl.getLayoutParams().height;
            final float w = rl.getLayoutParams().width;

            rl.animateRipple(w / 2, h / 2);

            rl.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                @Override
                public void onComplete(RippleView rippleView) {

                    rl.animateRipple(w / 2, h / 2);
                }
            });

            final MSBeacon beacon = beacons.get(position);

            final TextView name = (TextView) view.findViewById(R.id.text_merchant);
            final RippleView rippleView = (RippleView) view.findViewById(R.id.rl);
            final TextView title = (TextView) view.findViewById(R.id.tvDialogTitle);
            final Button btn_Close = (Button) view.findViewById(R.id.btnClose);

            btn_Close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (beaconSearchDialog != null)
                        beaconSearchDialog.dismiss();
                }
            });

            GetMerchantByUUIDRequest request = new GetMerchantByUUIDRequest();
            request.setMerchantUid(beacon.getBeaconKey());

            AppWs.getMerchantDetailsByUUID(request, ctx, new AppWs.WsListener() {

                @Override
                public void onResponseSuccess(final BaseResponse baseResponse) {

                    if (baseResponse instanceof GetMerchantByUUIDResponse) {

                        final GetMerchantByUUIDResponse merchantByUUIDResponse = (GetMerchantByUUIDResponse) baseResponse;
                        name.setText(merchantByUUIDResponse.getMerchantBean().getMerchantName() + "\n\n"+
                                        merchantByUUIDResponse.getMerchantBean().getShortAddress() );
                        name.setVisibility(View.VISIBLE);
                        rippleView.setVisibility(View.GONE);
                        title.setText(R.string.text_beacon_found_title);

                        name.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                TextView textView = (TextView) view;
                                etPaymentAddress.setText(merchantByUUIDResponse.getMerchantBean().getMerchantDefaultHandle());

                                if (beaconSearchDialog != null)
                                    beaconSearchDialog.dismiss();
                            }
                        });
                    }
                }

                @Override
                public void notifyResponseFailed(String message, BaseRequest request) {

                }
            });

       /* TextView key = (TextView) view.findViewById(R.id.device_address);
        key.setText("Major: " + beacon.getMajor() + "\t\t\t Minor: " + beacon.getMinor() +
            " \t\t\t  Mean RSSI: " + beacon.getMeanRSSI());*/

       /* if (beacon.getIsCampedOn()) {
            view.setBackgroundResource(android.R.color.holo_green_light);
        } else {
            view.setBackgroundResource(android.R.color.background_light);
        }*/

            return view;
        }
    }

}