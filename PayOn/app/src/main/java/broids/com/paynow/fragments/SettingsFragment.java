package broids.com.paynow.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import broids.com.paynow.R;
import broids.com.paynow.utils.Base64ImageUtil;
import broids.com.paynow.ws.AppWs;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;

public class SettingsFragment extends BaseFragment implements AppWs.WsListener, View.OnClickListener {

    public static SettingsFragment getInstance() {

        SettingsFragment fragment = new SettingsFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int getContentView() {

        return R.layout.fragment_settings;
    }

    @Override
    protected void initViews(View rootView) {

        ImageView iv = (ImageView) rootView.findViewById(R.id.iv);
        byte[] bytes = new byte[0];
        try {

            InputStream is = activity.getAssets().open("sample_base_64.txt");
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();

            bytes = buffer.toByteArray();;

            Bitmap bitmap = Base64ImageUtil.bytesToBitmap(bytes);
            iv.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void setTextLabels() {

    }

    @Override
    protected void setAppTheme() {

    }

    @Override
    protected String getTitle() {

        String title = activity.getResources().getString(R.string.home);
        return title;
    }

    @Override
    protected void loadData() {



    }

    @Override
    public void onResponseSuccess(BaseResponse baseResponse) {

        dismissProgressDialog();
    }

    @Override
    public void notifyResponseFailed(String message, BaseRequest request) {

        dismissProgressDialog();
    }

    @Override
    public void onClick(View v) {

    }
}
