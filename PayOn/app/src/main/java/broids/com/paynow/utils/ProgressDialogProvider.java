package broids.com.paynow.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import broids.com.paynow.R;

public class ProgressDialogProvider {

    /*public static ProgressDialog getSimpleProgressDialog(int titleResource, int messageResource, Activity activity) {

        String strTitle = activity.getString(titleResource);

        String strMessage = activity.getString(messageResource);

        return getSimpleProgressDialog(strTitle, strMessage, activity);
    }*/

    /*public static ProgressDialog getSimpleProgressDialog(String title, String message, Activity activity) {

        ProgressDialog progressDialog = new ProgressDialog(activity);

        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(true);

        return progressDialog;
    }*/

    public static Dialog getSimpleProgressDialog(Activity activity) {

        /*ProgressDialog progressDialog = new ProgressDialog(activity);

        progressDialog.setCancelable(true);*/

        Dialog dialog = new  Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.progress_view);

        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.rotate);

        View view = dialog.findViewById(R.id.iv);
        view.startAnimation(anim);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return dialog;
    }



}
