package broids.com.paynow.ws;

import android.content.Context;

import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;

import broids.com.paynow.entity.MerchantBean;
import broids.com.paynow.utils.Logger;
import broids.com.paynow.utils.NetworkUtils;
import broids.com.paynow.ws.entity.BaseRequest;
import broids.com.paynow.ws.entity.BaseResponse;
import broids.com.paynow.ws.entity.GetMerchantByUUIDRequest;
import broids.com.paynow.ws.entity.GetMerchantByUUIDResponse;
import broids.com.paynow.ws.entity.LoginRequest;
import broids.com.paynow.ws.entity.LoginResponse;
import broids.com.paynow.ws.entity.RegisterRequest;
import broids.com.paynow.ws.entity.RegisterResponse;
import broids.com.paynow.ws.entity.TransactResponse;
import broids.com.paynow.entity.TransactionBean;
import broids.com.paynow.ws.entity.TransactionHistoryRequest;
import broids.com.paynow.ws.entity.TransactionHistoryResponse;
import broids.com.paynow.ws.retrofit.RestClient;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class AppWs {

    private static final String TAG = "AppWs";

    public static void login(final LoginRequest request, final Context context, final WsListener listener) {

        Call<LoginResponse> call = RestClient.get().login(request);

        try {

            call.enqueue(new Callback<LoginResponse>() {

                @Override
                public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {

                    LoginResponse baseResponse = response.body();

                    if (baseResponse != null
                            && response.isSuccess()) {

                        if (listener != null) {

                            listener.onResponseSuccess(baseResponse);
                        }

                    } else {

                        if (listener != null) {

                            ResponseBody errorBody = response.errorBody();

                            if (errorBody != null) {

                            }

                            listener.notifyResponseFailed(null, request);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                    retrofitError(t, listener, context);

                    listener.notifyResponseFailed(null, request);
                }
            });

        } catch (Exception e) {

            Logger.e(e);

            listener.notifyResponseFailed(null, request);
        }
    }

    public static void transact(final TransactionBean request, final Context context, final WsListener listener) {

        Call<TransactResponse> call = RestClient.get().transact(request);

        try {

            call.enqueue(new Callback<TransactResponse>() {

                @Override
                public void onResponse(Response<TransactResponse> response, Retrofit retrofit) {

                    TransactResponse baseResponse = response.body();

                    if (baseResponse != null
                            && response.isSuccess()) {

                        if (listener != null) {

                            listener.onResponseSuccess(baseResponse);
                        }

                    } else {

                        if (listener != null) {

                            ResponseBody errorBody = response.errorBody();

                            if (errorBody != null) {

                            }

                            listener.notifyResponseFailed(null, null);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                    retrofitError(t, listener, context);

                    listener.notifyResponseFailed(null, null);
                }
            });

        } catch (Exception e) {

            Logger.e(e);

            listener.notifyResponseFailed(null, null);
        }
    }

    public static void getTransactionHistory(final TransactionHistoryRequest request, final Context context, final WsListener listener) {

        Call<ArrayList<TransactionBean>> call = RestClient.get().getTransactionHistory(request);

        try {

            call.enqueue(new Callback<ArrayList<TransactionBean>>() {

                @Override
                public void onResponse(Response<ArrayList<TransactionBean>> response, Retrofit retrofit) {

                    ArrayList<TransactionBean> history = response.body();

                    TransactionHistoryResponse baseResponse = new TransactionHistoryResponse();
                    baseResponse.setHistory(history);
                    baseResponse.setResponse("Success");

                    if (baseResponse != null
                            && response.isSuccess()) {

                        if (listener != null) {

                            listener.onResponseSuccess(baseResponse);
                        }

                    } else {

                        if (listener != null) {

                            ResponseBody errorBody = response.errorBody();

                            if (errorBody != null) {

                            }

                            listener.notifyResponseFailed(null, request);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                    retrofitError(t, listener, context);

                    listener.notifyResponseFailed(null, request);
                }
            });

        } catch (Exception e) {

            Logger.e(e);

            listener.notifyResponseFailed(null, request);
        }
    }

    public static void getMerchantDetailsByUUID(final GetMerchantByUUIDRequest request, final Context context, final WsListener listener) {

        Call<ArrayList<MerchantBean>> call = RestClient.get().getMerchantDetailsByUUID(request);

        try {

            call.enqueue(new Callback<ArrayList<MerchantBean>>() {

                @Override
                public void onResponse(Response<ArrayList<MerchantBean>> response, Retrofit retrofit) {


                    ArrayList<MerchantBean> merchantBeans = response.body();

                    GetMerchantByUUIDResponse baseResponse = new GetMerchantByUUIDResponse();

                    for(MerchantBean merchantBean : merchantBeans) {

                        baseResponse.setMerchantBean(merchantBean);

                    }

                    if (baseResponse != null
                            && response.isSuccess()) {

                        if (listener != null) {

                            listener.onResponseSuccess(baseResponse);
                        }

                    } else {

                        if (listener != null) {

                            ResponseBody errorBody = response.errorBody();

                            if (errorBody != null) {

                            }

                            listener.notifyResponseFailed(null, request);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                    retrofitError(t, listener, context);

                    listener.notifyResponseFailed(null, request);
                }
            });

        } catch (Exception e) {

            Logger.e(e);

            listener.notifyResponseFailed(null, request);
        }
    }

    public static void register(final RegisterRequest request, final Context context, final WsListener listener) {

        Call<RegisterResponse> call = RestClient.get().register(request);

        try {

            call.enqueue(new Callback<RegisterResponse>() {

                @Override
                public void onResponse(Response<RegisterResponse> response, Retrofit retrofit) {

                    RegisterResponse baseResponse = response.body();

                    if (baseResponse != null
                            && response.isSuccess()) {

                        if (listener != null) {

                            listener.onResponseSuccess(baseResponse);
                        }

                    } else {

                        if (listener != null) {

                            ResponseBody errorBody = response.errorBody();

                            if (errorBody != null) {

                            }

                            listener.notifyResponseFailed(null, request);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {

                    retrofitError(t, listener, context);

                    listener.notifyResponseFailed(null, request);
                }
            });

        } catch (Exception e) {

            Logger.e(e);

            listener.notifyResponseFailed(null, request);
        }
    }

    public static void retrofitError(Throwable t, WsListener listener, Context context) {

        if (listener != null) {
            Logger.e(t);
            listener.notifyResponseFailed(null, null);
            NetworkUtils.checkInternetConnection(context);
        }
    }

    public interface WsListener {

        void onResponseSuccess(BaseResponse baseResponse);

        void notifyResponseFailed(String message, BaseRequest request);
    }

}