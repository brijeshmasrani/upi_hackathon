package broids.com.paynow.ws.entity;

import android.content.Context;

import com.aadhaarconnect.bridge.gateway.model.Kyc;
import com.aadhaarconnect.bridge.gateway.model.KycResponse;
import com.aadhaarconnect.bridge.gateway.model.PoiType;
import com.google.gson.Gson;

import broids.com.paynow.utils.AppPreferences;
import broids.com.paynow.utils.Base64ImageUtil;
import broids.com.paynow.utils.StringUtils;

/**
 * Created by NIKHIL on 3/20/2016.
 */
public class AadhaarKycBean {

    private static final String AADHAAR_KYC = "aadhaar_kyc";

    private AadhaarKycBean() {

    }

    private String aadhaar;
    private String name;
    private String email;
    private String phone;
    private String imageBase64;

    public String getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(String aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public static void save(KycResponse response, Context context) {

        if (response == null) {

            return;
        }

        Kyc kyc = response.getKyc();

        if (kyc == null) {

            return;
        }

        PoiType poiType = kyc.getPoi();

        String aadhaar = kyc.getAadhaar();

        String name = "";
        String phone = "";
        String email = "";

        if (poiType != null) {

            name = poiType.getName();
            email = poiType.getEmail();
            phone = poiType.getPhone();
        }

        byte[] photo = kyc.getPhoto();
        String base64Photo = Base64ImageUtil.bitmapToString(photo);

        AadhaarKycBean aadhaarKycBean = new AadhaarKycBean();

        aadhaarKycBean.setAadhaar(aadhaar);
        aadhaarKycBean.setEmail(email);
        aadhaarKycBean.setName(name);
        aadhaarKycBean.setPhone(phone);
        aadhaarKycBean.setImageBase64(base64Photo);

        Gson gson = new Gson();
        String json = gson.toJson(aadhaarKycBean);

        AppPreferences.setString(AADHAAR_KYC, json, context);
    }

    public static AadhaarKycBean getKyc(Context context) {

        String json = AppPreferences.getString(AADHAAR_KYC, "", context);

        if(!StringUtils.isEmpty(json)) {

            Gson gson = new Gson();

            AadhaarKycBean aadhaarKyc = gson.fromJson(json, AadhaarKycBean.class);
            return aadhaarKyc;
        }

        return null;
    }
}
