package broids.com.paynow.ws.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NIKHIL on 3/20/2016.
 */
public class GetMerchantByUUIDRequest extends BaseRequest {

    @SerializedName("merchant_uid")
    private String merchantUid;

    public String getMerchantUid() {
        return merchantUid;
    }

    public void setMerchantUid(String merchantUid) {
        this.merchantUid = merchantUid;
    }
}
