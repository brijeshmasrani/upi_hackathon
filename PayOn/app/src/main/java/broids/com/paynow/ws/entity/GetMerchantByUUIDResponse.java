package broids.com.paynow.ws.entity;

import broids.com.paynow.entity.MerchantBean;

/**
 * Created by NIKHIL on 3/20/2016.
 */
public class GetMerchantByUUIDResponse extends BaseResponse {

    private MerchantBean merchantBean;

    public MerchantBean getMerchantBean() {
        return merchantBean;
    }

    public void setMerchantBean(MerchantBean merchantBean) {
        this.merchantBean = merchantBean;
    }
}
