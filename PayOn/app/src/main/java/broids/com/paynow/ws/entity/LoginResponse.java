package broids.com.paynow.ws.entity;

import com.google.gson.annotations.SerializedName;


public class LoginResponse extends BaseResponse {

    @SerializedName("Response")
    private String response;

    @SerializedName("user_id")
    private String userId;

    public String getResponse() {

        return response;
    }

    public void setResponse(String response) {

        this.response = response;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
