package broids.com.paynow.ws.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by brijesh on 27/2/16.
 */
public class RegisterRequest extends BaseRequest {

    private String email;

    private String password;

    @SerializedName("phone_number")
    private String phoneNumber;

    @SerializedName("device_id")
    private String deviceId;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("aadhar_number")
    private String aadharNumber;

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getPhoneNumber() {

        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    public String getDeviceId() {

        return deviceId;
    }

    public void setDeviceId(String deviceId) {

        this.deviceId = deviceId;
    }

    public String getFirstName() {

        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public String getAadharNumber() {

        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {

        this.aadharNumber = aadharNumber;
    }
}
