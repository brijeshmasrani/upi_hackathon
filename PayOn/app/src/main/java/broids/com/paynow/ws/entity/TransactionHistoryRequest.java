package broids.com.paynow.ws.entity;

import com.google.gson.annotations.SerializedName;

public class TransactionHistoryRequest extends BaseRequest {

    @SerializedName("user_id")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
