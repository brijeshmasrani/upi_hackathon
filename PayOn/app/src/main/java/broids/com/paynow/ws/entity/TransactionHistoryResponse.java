package broids.com.paynow.ws.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import broids.com.paynow.entity.TransactionBean;

/**
 * Created by NIKHIL on 3/20/2016.
 */
public class TransactionHistoryResponse extends BaseResponse {

    @SerializedName("History")
    private ArrayList<TransactionBean> history = new ArrayList<>();

    @SerializedName("Response")
    private String response;

    public ArrayList<TransactionBean> getHistory() {
        return history;
    }

    public void setHistory(ArrayList<TransactionBean> history) {
        this.history = history;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
