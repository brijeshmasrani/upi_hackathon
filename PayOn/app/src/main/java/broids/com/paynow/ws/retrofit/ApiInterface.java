package broids.com.paynow.ws.retrofit;

import java.util.ArrayList;

import broids.com.paynow.entity.MerchantBean;
import broids.com.paynow.ws.entity.GetMerchantByUUIDRequest;
import broids.com.paynow.ws.entity.GetMerchantByUUIDResponse;
import broids.com.paynow.ws.entity.LoginRequest;
import broids.com.paynow.ws.entity.LoginResponse;
import broids.com.paynow.ws.entity.RegisterRequest;
import broids.com.paynow.ws.entity.RegisterResponse;
import broids.com.paynow.ws.entity.TransactResponse;
import broids.com.paynow.entity.TransactionBean;
import broids.com.paynow.ws.entity.TransactionHistoryRequest;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("login")
    Call<LoginResponse> login(@Body LoginRequest request);

    @Headers("Content-Type: application/json")
    @POST("register")
    Call<RegisterResponse> register(@Body RegisterRequest request);

    @Headers("Content-Type: application/json")
    @POST("getTransactionHistory")
    Call<ArrayList<TransactionBean>> getTransactionHistory(@Body TransactionHistoryRequest request);

    @Headers("Content-Type: application/json")
    @POST("getMerchantDetailsByUID")
    Call<ArrayList<MerchantBean>> getMerchantDetailsByUUID(@Body GetMerchantByUUIDRequest request);

    @Headers("Content-Type: application/json")
    @POST("transact")
    Call<TransactResponse> transact(@Body TransactionBean request);

}